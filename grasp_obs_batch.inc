<?php

function grasp_obs_batch(){
	return drupal_get_form("grasp_obs_batch_form");
}

function grasp_obs_batch_form($form,&$form_state){
	$form = array();

	$form['csvupload'] = array(
		'#title' => t('Upload observations csv file'),
		'#type' => 'managed_file',
		'#description' => t('The csv file must respect the standard for this project.'),
		'#upload_validators' => array('file_validate_extensions' => array('csv') ),
		'#upload_location' => 'public://',
	);

	$form['indicator_desc'] = array(
		'#type' => 'checkbox',
	  '#title' => t('Override indicator description if exists.'),
	);

	$form['submit'] = array('#type' => 'submit', '#weight' => 20, '#value' => t('Verify and insert data'));

	return $form;
}

function grasp_obs_batch_form_submit($form, &$form_state) {
	$batch = array(
    'title' => t('Importing CSV ...'),
    'operations' => array(),
    'init_message' => t('Importing'),
#    'progress_message' => t('Processed @current out of @total.'),
    'error_message' => t('An error occurred during processing'),
    'finished' => 'grasp_import_finished',
    'file' => drupal_get_path('module', 'grasp') .'/grasp_obs_batch.inc',
  ) ;

	if ( isset( $form_state['values']['csvupload'] ) ) {
    // set the filename
    $batch['operations'][] = array('_grasp_set_filename', array($form_state['values']['csvupload'], $form_state['values']['indicator_desc']) );
		// get the headers and set parameters
		$batch['operations'][] = array('_grasp_get_header', array() );
		// verify and create observed properties, unit of measure and offering
			// $batch['operations'][] = array('_grasp_verifycreate_environment', array() );
		$batch['operations'][] = array('_grasp_verify_service', array() );
		$batch['operations'][] = array('_grasp_verifycreate_uom', array() );
		$batch['operations'][] = array('_grasp_verifycreate_offering', array() );
		$batch['operations'][] = array('_grasp_verifycreate_op', array() );

		// verify and create procedure
		$batch['operations'][] = array('_grasp_verifycreate_prc', array() );
		// verify and insert procedure inside the right offering
		$batch['operations'][] = array('_grasp_moveprcto_offering', array() );
		// Once the procedure is created
		// 1 - getprocedure details to obtain the assigned sensor id
		// 2 - getobservation last to obtain the field order inside the procedure
		$batch['operations'][] = array('_grasp_set_insertobservations_param', array() ) ;
		// Insert values
		$batch['operations'][] = array('_grasp_insert_observations', array());
		// Create the Indicator if the id_type is i and the indicator does not exist
		$batch['operations'][] = array('_grasp_create_indicator', array());
	}

  batch_set($batch);
}

function _grasp_set_filename($filename, $override, &$context){
	$context['results']['uploaded_filename'] = $filename;
	$context['results']['override_ind_desc'] = $override;
}


function _grasp_get_header(&$context){
	$filename = $context['results']['uploaded_filename'];
	$file = file_load($filename);

	if ( $handle = fopen(drupal_realpath($file->uri), 'r') ) {
		while ( ($data = fgetcsv($handle, 4096, ',')) != FALSE){
			switch ($data[0]){
				case 'name':
					// The name to aasign to the procedure
 					$context['results']['prc_name'] = $data[1];
					break;
					// If the id type is I - Indicator - create a content of type Inticator
				case 'id_type':
					$context['results']['id_type'] = strtolower($data[1]);
					break;
				// The short label of the marker/FOI this procedure will get the geographical references
				// This parameter has to be matched with a marker short label
				case 'marker_id':
					$context['results']['marker_id'] = $data[1];
					break;
				// The description to assign to the procedure
				case 'description':
					$context['results']['description'] = $data[1];
					break;
				// The unit of measure - If it does not exist in istsos create it
				case 'unit':
					$context['results']['unit'] = $data[1];
					break;
				// the frequency is relatet with the istsos offering
				// Verify if the offering exists - create it if not
				case 'frequency':
					$context['results']['frequency'] = $data[1];
					break;
				// This must exist and has to be a istsos service
				case 'scenario':
					$context['results']['service'] = $data[1];
					break;
				// time with observed properties
				case 'timestart':
					// we have to fill the $context['results']['ob_prop'] array with just one time observed property
					if ($data[1] == 'timeend'){
						array_shift($data);
						$context['results']['doubletime'] = true; // to perform the read line of insertobservation
						$context['results']['ob_prop'] = $data;
						$context['results']['ob_prop'][0] = 'Time';
					} else {
						$context['results']['doubletime'] = false;
						$context['results']['ob_prop'] = $data;
						$context['results']['ob_prop'][0] = 'Time';
					}
					$context['results']['offset'] = ftell($handle);
					break 2;
			}
		}
		$context['results']['shift'] = ftell($handle);
		fclose($handle);
	}
}

function _grasp_verify_service(&$context){
	$context['results']['error'] = FALSE;
	// Get the config section for the service
	// If the service does not exist set an error
	$config = istsos_REST_configsections($context['results']['service']);
	if ($config !== false){
		$context['results']['urn'] = $config['data']['urn'];
	} else {
		drupal_set_message('ERROR getting information for istsos config section - see log for details.<br \> Verify if the service <b>'.$context['results']['service'].'</b> exist in istsos.', 'error');
		$context['results']['error'] = TRUE;
		// $context['results']['skip'] = TRUE;
	}
}
function _grasp_verifycreate_uom(&$context){
	if (!$context['results']['error']){
		// $uom = $context['results']['unit'];
		// Unit of measure
		$getuoms = istsos_REST_getuoms($context['results']['service']);
		if ($getuoms === false){
			drupal_set_message('ERROR getting unit of measure information - see log for details', 'error');
			$context['results']['error'] = TRUE;
		} else {
			foreach ($getuoms['data'] as $values){
				$uoms_in[] = $values['name'];
			}
			$new_uom = array_diff(array($context['results']['unit']), $uoms_in);
			if (!empty ($new_uom[0])){
				$insert_uom = array ('name' => $new_uom[0], 'description' => '');
				$ins_uom_resp = istsos_REST_insertuom ($context['results']['service'], $insert_uom);
				if($ins_uom_resp === false){
					drupal_set_message('ERROR insert unit of measure - see log for details', 'error');
					$context['results']['error'] = TRUE;
				}
			}
		}
	}
}

function _grasp_verifycreate_offering(&$context){
	if (!$context['results']['error']){
		// offering
		$url     = variable_get('istsos_url');
		$offering_list = istsos_REST_offeringlist($url, $context['results']['service']);
		if ($offering_list === false){
			drupal_set_message('ERROR getting list of offerings - see log for details', 'error');
			$context['results']['error']['offeringlist'] = TRUE;
		} else {
			$offerings_in = array();
			foreach ($offering_list['data'] as $values){
				$offerings_in[] = $values['name'];
			}
			$new_offe = array_diff(array($context['results']['frequency']), $offerings_in);
			if (!empty ($new_offe[0])){
				$insert_offe = array ('name' => $new_offe[0], 'description' => '' , 'expiration' => '', 'active' => 'true');
				$ins_off_resp = istsos_REST_insertoffering ( $url, $context['results']['service'], $insert_offe);
				if($ins_off_resp === false){
					drupal_set_message('ERROR inserting new offering - see log for details', 'error');
					$context['results']['error'] = TRUE;
				}
			}
		}
	}
}

function _grasp_verifycreate_op(&$context){
	if (!$context['results']['error']){
		// observed properties
		$obs_prop = $context['results']['ob_prop'];
		// shift the array in order to remove the time index - this way when performing the insert of new observed properties the paramter Time is out
		array_shift($obs_prop);
		$op_list =  istsos_REST_getop($context['results']['service']);
		if ($op_list === false){
			drupal_set_message('ERROR getting observed properties list - see log for details', 'error');
			$context['results']['error'] = TRUE;
		} else {
			foreach ($op_list['data'] as $values){
				$appo = explode(':', $values['name']);
				if(!empty($appo)){
					$op_in[] = $appo[0];
				}
			}
			$new_op = array_diff($obs_prop, $op_in);
			if (!empty ($new_op)){
				foreach($new_op as $value){
					$name = $value.':'.$value;
					$op = array('name' => $name, 'definition' => $context['results']['urn']['parameter'].$name, 'description' => '', 'constraint' => '');
					$ins_op_resp = istsos_REST_insertop ($context['results']['service'], $op);
					if($ins_op_resp === false){
						drupal_set_message('ERROR insert observed property - see log for details', 'error');
						$context['results']['error'] = TRUE;
					}
				}
			}
		}
	}
}

function _grasp_verifycreate_prc (&$context) {
	if (!$context['results']['error']){
		$prc_name = $context['results']['prc_name'];
		$marker_id = $context['results']['marker_id'];
		$description = $context['results']['description'];
		$obs_prop = $context['results']['ob_prop'];
		$service  = $context['results']['service'];
		// shift the array in order to remove the time index
		// when the procedure is created the time output is insert by default
		array_shift($obs_prop);

		$prc_details  = istsos_REST_proceduredetails (variable_get('istsos_url'), $service, $prc_name);
		if (!$prc_details){
			$query = new EntityFieldQuery();
			$query->entityCondition('entity_type', 'node')
				->entityCondition('bundle', 'marker')
				->propertyCondition('status', 1)
				->fieldCondition('field_short_label', 'value', $marker_id, '=');
			$result = $query->execute();
			if (isset($result['node'])) {
				$news_items_nids = array_keys($result['node']);
				// Store the node id of the marker content
				$context['results']['marker_nid'] = $news_items_nids[0];
				// create the outputs array
				foreach ($obs_prop as $name){
					$outputs[] = array(
						"name" => $name.':'.$name,
				    "definition" => $context['results']['urn']['parameter'].$name.':'.$name,
				    "uom" => $context['results']['unit'],
				    "description" => "",
				 	);
				}
				$add_proc_resp = grasp_REST_insertprocedure($service, $prc_name, $description, $news_items_nids[0], $outputs);
				if ($add_proc_resp !== false){
					$context['results']['procedure'] = $add_proc_resp;
				} else {
					drupal_set_message('ERROR creating the procedure - see log for details', 'error');
					$context['results']['error'] = TRUE;
				}
			} else {
				$context['results']['error'] = TRUE;
				drupal_set_message('ERROR inserting the procedure - verify the marker with short label '.$marker_id.' exist.', 'error');
				$context['results']['marker_nid'] = FALSE;
			}
		}
	}
}

function _grasp_moveprcto_offering (&$context){
	if (!$context['results']['error']){
		$prc_name  = $context['results']['prc_name'];
		$offe_name = $context['results']['frequency'];
		$service  = $context['results']['service'];
		$isout = true;
		$memberlist_resp = istsos_REST_prcmemberlist ( variable_get('istsos_url'), $service, $offe_name);
		if ($memberlist_resp !== false){
			foreach ($memberlist_resp['data'] as $values){
				if ($values['name'] == $prc_name ){
					$isout = false;
				}
			}
			if ($isout){
				$response = istsos_REST_addprctooffe ($service, $offe_name, $prc_name);
				if ($response !== false) {
					$context['results']['prctooffe'] = true;
				} else {
					$context['results']['prctooffe'] = false;
				}
			} else {
				$context['results']['prctooffe'] = true;
			}
		} else {
			drupal_set_message('ERROR getting procedures memberlist for the given offering - see log for details', 'error');
			$context['results']['error'] = TRUE;
		}
	}
}


function _grasp_set_insertobservations_param (&$context) {
	if (!$context['results']['error']){
		$prc_name = $context['results']['prc_name'];
		$service  = $context['results']['service'];
		// describesensor request
		$sensor_desc = istsos_REST_proceduredetails (variable_get('istsos_url'), $service, $prc_name);
		if ($sensor_desc !== false){
			$context['results']['aid'] = $sensor_desc['data']['assignedSensorId'];
			// get urn for parameter
			$urn_auth = $context['results']['urn']['parameter'];
			// 3 getobservation last on temporary offering to get the data skeleton and observed properties disposition
			$getobs_res = istsos_REST_getobservation ( variable_get('istsos_url'), $service, 'temporary', $prc_name, $urn_auth, 'last');
			if ($getobs_res !== false ){
				$context['results']['skeleton'] = $getobs_res['data'][0];
				//create fixed context array with ordered elements field from the getobservation request
				foreach ($context['results']['skeleton']['result']['DataArray']['field'] as $index => $dnu) {
					if ($index !== 0) { // jump time element
						if ($index & 1){ // is not quality index
							//
							//  Create an array $context['results']['order'] from the 'names' returned from the get observation
							//  This 'names' will be compared with the names passed inside the file
							//  $context['results']['order']['C100'] = 1;
							//
							$array = explode(':', $dnu['name']); // modify this in order to compare on different name from file
							$context['results']['order'][$array[0]] = $index; // flip the array $order['C100'] = 1
						}
					} else {
						$context['results']['order']['Time'] = 0;
					}
				}
			} else {
				drupal_set_message('ERROR getting observation - see log for details', 'error');
				$context['results']['error'] = TRUE;
			}
		} else {
			drupal_set_message('ERROR getting procedure details - see log for details', 'error');
			$context['results']['error'] = TRUE;
		}
	}
}

function _grasp_insert_observations (&$context) {
	if (!$context['results']['error']){
		if (!isset($context['sandbox']['offset'])) {
		  $context['sandbox']['offset'] = $context['results']['offset'];
		  $context['sandbox']['records'] = 0;
		}

		$partial = array();
		$file = file_load($context['results']['uploaded_filename']);
		if ( $handle = fopen(drupal_realpath($file->uri), 'r') ) {

			$ret = fseek($handle, $context['sandbox']['offset']);
			if ( $ret != 0 ) {
				// Failed to seek
				watchdog('grasp csv batch', 'Failed to seek to ' . $context['sandbox']['offset']);
				$context['finished'] = TRUE;
				return;
			}

			// this limit influence the postgresql cpu usage
			// with limit = 5 it reach 30% of cpu usage
			// with limit = 50 it reach 70% of cpu usage
			$limit = 20;  // Maximum number of rows to process at a time
			$done = FALSE;

			for ( $i = 0; $i < $limit; $i++ ) {
				$line = fgetcsv($handle, 4096, ',');
				if ( $line === FALSE ) {
					$done = TRUE;
					// No more records to process
					break;
				}	else {
					if ($context['results']['doubletime']){
						array_shift($line); // Depending on the file a line may contains 1 timestamp or 2 timestamp
					}
					// skip eventual new lines at the end of the file
					if (count ($line) == count ($context['results']['order']) ){
						$days_before_epoch = 719529-$line[0];
						$unixDate = -$days_before_epoch * 86400;
						$line[0] = date("c", $unixDate);

						$context['sandbox']['records']++;
						$context['sandbox']['offset'] = ftell($handle);
						$record = $context['sandbox']['records'];
						$partial[] = $line;
					}
				}
			}
			// insert observations must respect the order of fields of a get observation request
			// drupal_set_message("<pre>".print_r($partial,TRUE)."</pre>");
			// drupal_set_message("<pre>".print_r($context['results']['skeleton'],TRUE)."</pre>");
			// drupal_set_message("<pre>".print_r($context['results']['order'],TRUE)."</pre>");
			// drupal_set_message("<pre>".print_r($context['results']['ob_prop'],TRUE)."</pre>");
			$ins_obs_resp = istsos_REST_insertobservation( variable_get('istsos_url'), $context['results']['service'], $context['results']['prc_name'], $context['results']['aid'], $partial, $context['results']['skeleton'], $context['results']['order'], $context['results']['ob_prop']);
			if ($ins_obs_resp !== false){
				$eof = feof($handle);
				if ( $eof )  {
					$context['success'] = TRUE;
				}
				$context['message'] = "Processed " . $context['sandbox']['records'] . " rows.";
				$context['finished'] = ( $eof || $done ) ? 1 : 0;
				fclose($handle);
			} else {
				drupal_set_message('ERROR insert observation - see log for details', 'error');
				$context['finished'] = TRUE;
				fclose($handle);
			}
		}
	}
}


function _grasp_create_indicator (&$context) {
	if (!$context['results']['error']){
		if ($context['results']['id_type'] === 'i'){
			$query = new EntityFieldQuery();
			$entities = $query->entityCondition('entity_type', 'node')
				->propertyCondition('type', 'indicator')
				->propertyCondition('title', $context['results']['prc_name'])
				->propertyCondition('status', 1)
				->range(0,1)
				->execute();

			if (empty($entities['node'])) {
				#	$node = node_load(array_shift(array_keys($entities['node'])));
				global $user;
				$node = new stdClass();
				$node->title = $context['results']['prc_name'];
				$node->type = "indicator";
				node_object_prepare($node); // Sets some defaults. Invokes hook_prepare() and hook_node_prepare().
				$node->language = LANGUAGE_NONE; // Or e.g. 'en' if locale is enabled
				$node->uid = $user->uid;
				$node->status = 1; //(1 or 0): published or not
				$node->promote = 0; //(1 or 0): promoted to front page
				$node->comment = 0; // 0 = comments disabled, 1 = read only, 2 = read/write

				$node->field_ind_desc_short[$node->language][0]['value'] = $context['results']['description'];

				$node = node_submit($node); // Prepare node for saving
				node_save($node);
				$context['results']['indicator'] = $node->nid;
			} elseif ($context['results']['override_ind_desc'] == 1) {
				$node = node_load(array_shift(array_keys($entities['node'])));
				$node->field_ind_desc_short[$node->language][0]['value'] = $context['results']['description'];
				$node = node_submit($node); // Prepare node for saving
				node_save($node);
			}
		}
	}
}

function grasp_REST_insertprocedure ($service, $prc_name, $description, $foi_nid, $outputs) {
	$url     = variable_get('istsos_url');
	/**/
	/* For drupal module we create a procedure with the coordinates of the FOI */
	/**/
	$foi = node_load($foi_nid);
#	drupal_set_message("FOI <pre>".print_r($foi,TRUE)."</pre>");
	/* Calculate the coordinates from the postgis field in the node */
	$n_type = $foi->type;
	// Get the postgis field name given the node type
	// TODO manage multiple PostGIS fields inside the same content type
	$field_info_map = field_info_field_map();
	foreach ($field_info_map as $key => $value){
		if ( (isset ($value['bundles']['node'])) && ($value['bundles']['node'][0] == $n_type) && ($value['type'] == 'postgis') ){
			$field_name = $key;
		}
	}
	$field_info = field_info_field($field_name);

	$srid = $field_info['settings']['srid'];

	$result = db_query("select ST_X(".$field_name."_geometry), ST_Y(".$field_name."_geometry), ST_AsText(".$field_name."_geometry) from field_data_".$field_name." where entity_id=".$foi_nid.";");
	$db_entry = $result->fetchAssoc();
#		drupal_set_message("db results <pre>".print_r($db_entry,TRUE)."</pre>");

	/* Create the array for procedure insert */
	$data['system_id'] = $prc_name; // Using the procedure name for this parameter as is using the istsos admin interface.
  $data['system'] = $prc_name;    // Procedure name
  $data['description'] = $description;// Description of the procedure
  $data['keywords'] = '';
  $data['identification'] = array();
  $data['classification']	= array(
  	array ("name" => "System Type", "definition" => "urn:ogc:def:classifier:x-istsos:1.0:systemType", "value" => "insitu-fixed-point"),
  	array ("name" => "Sensor Type", "definition" => "urn:ogc:def:classifier:x-istsos:1.0:sensorType", "value" => "-"),
  );
  $data['characteristics'] = "";
  $data['contacts'] = array();
  $data['documentation'] = array();
	$data['location'] = array (
          "type" => "Feature",
          "geometry" => array(
              "type" => "Point",
              "coordinates" => array($db_entry['st_y'],$db_entry['st_x'],100),
          ),
          "crs" => array (
              "type" => "name",
							 "properties" => array("name" => $srid),
          ),
          "properties" => array("name" => $foi->field_short_label['und'][0]['value']) // TODO the short label of the FOI is related to IMRR project
#          "properties" => array("name" => 'title') //  insert the node title that is the FOI name
   );
	$data['interfaces'] = "";
  $data['inputs'] = array();
  $data['history'] =  array();
  $data['capabilities'] =  array();

	$data['outputs'] = array();
  $time = array(
              "name" => "Time",
							"definition" => "urn:ogc:def:parameter:x-istsos:1.0:time:iso8601",
              "uom" => "iso8601",
              "description" => "",
  );
	array_push($data['outputs'], $time);

	 /* Insert new outputs */
  foreach ($outputs as $value ){
		$output = array(
			"name" => $value['name'],
			"definition" => $value['definition'],
			"uom" => $value['uom'],
			"description" => $value['description'],
		);
		array_push($data['outputs'], $output);
  }

	/* POST operation to ADD the procedure */
	$options = array(
    'method' => 'POST',
    'data' => drupal_json_encode($data),
    'headers' => array('Content-Type' => 'application/json'),
  );

  $add_procedure = drupal_http_request($url.'/wa/istsos/services/'.$service.'/procedures', $options);
	return istsos_response_parse($add_procedure, 'grasp_REST_insertprocedure');
}


function grasp_import_finished($success, $results, $operations) {
	// drupal_set_message("Success <pre>".print_r($success,TRUE)."</pre>");
	// drupal_set_message("Results <pre>".print_r($results,TRUE)."</pre>");
	// drupal_set_message("Operations <pre>".print_r($operations,TRUE)."</pre>");
	if ($success){
		drupal_set_message("Import CSV finished.");
		if (isset($results['procedure'])){
			drupal_set_message("Created new procedure ".$results['procedure']['message']);
		}
		if (isset($results['indicator'])){
			drupal_set_message("Created new indicator");
		}

	}
}
