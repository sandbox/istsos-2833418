<?php

/**/
/* d3 graph for Project tree callback */
/**/
function project_json_page($nid) {

		//  drupal_set_message("json page <pre>".print_r($nid,TRUE)."</pre>");

		$menu_name = "menu-project-structure";
		$menu = menu_load_links($menu_name);

		if ($nid == 'root') {
			$tree = menu_build_tree($menu_name, array());
			$root_name = 'Sectors';
		} else {
			/* Get the parent menu entry given the path */
			$path = "node/".$nid;
			$parent = menu_link_get_preferred($path, $menu_name);
			$root_name = $parent ['link_title'];

	    /* Create a parent list plid from the menu leaf that have the p1 parent the mlid of $parent */
	    $parents_list = array();
	    foreach ($menu as $item) {
	    	if ( !$item['has_children'] && $item['p1'] == $parent['mlid']){
	    		for ($i =1; $i<=$item['depth']; $i++){
	    			$parents_list[] = $item['p'.$i];
	    		}
	    		$parents_list = array_unique($parents_list); // reset parents values
	    	}
	    }
	    /* This is the parents list array to pass to expanded parameter in menu_build_tree */
	    $parents_list = array_values($parents_list); // reset keys

			$tree = menu_build_tree($menu_name, array( 'expanded' => $parents_list ));
		}
		$new_tree = D3_tree_menu_output($tree);

		$jall = array("name" => $root_name, "children" => $new_tree);
		drupal_json_output($jall);
}

/**/
/* Function to return the tree for D3 style graph */
/**/
function D3_tree_menu_output($tree) {
  $build = array();
  $items = array();

  // Pull out just the menu links we are going to render so that we
  // get an accurate count for the first/last classes.
  foreach ($tree as $data) {
    if ($data['link']['access'] && !$data['link']['hidden']) {
      $items[] = $data;
    }
  }

  foreach ($items as $i => $data) {
		/*  from menu node module */
		$node = menu_node_get_node($data['link']['mlid']);
		$element['name'] = $data['link']['title'];
		$element['type'] = node_type_get_name($node); // Sector - Level - Indicator
		$element['href'] = $data['link']['href'];
		/**/
		/* Example if we want to extract other values from node fields to pass back in the json response */
		/**/
    // if ( isset($node->field_top_label['und'][0]['value']) ){
    // 	$element['label'] = $node->field_top_label['und'][0]['value'];
    // }	else {
    // 	$element['label'] = "nan";
    // }
    $element['children'] = $data['below'] ? D3_tree_menu_output($data['below']) : $data['below'];
    if ( empty($element['children']) ){
    	unset($element['children']);
    }
    $build[] = $element;
  }
  return $build;
}
