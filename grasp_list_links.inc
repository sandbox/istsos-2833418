<?php

// This functions are used to show a list of menu links when you click on the root menu item for :
// Special Pages
function special_pages_grasp(){
	$item = menu_get_item();
  $content = system_admin_menu_block($item);
  return theme('node_add_list', array('content' => $content));
}
// Documentation
function documentation_pages_grasp() {
	$item = menu_get_item();
  $content = system_admin_menu_block($item);
  return theme('node_add_list', array('content' => $content));
}
