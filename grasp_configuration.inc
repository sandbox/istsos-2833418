<?php

function grasp_configuration () {
  return drupal_get_form('grasp_configuration_form');
}

function grasp_configuration_form($form, &$form_state) {

  $maps = openlayers_maps();
  // drupal_set_message('<pre>'.print_r($maps,TRUE).'</pre>');
  foreach($maps as $maps_names => $map_obj){
    $options[$maps_names] = $maps_names;
  }

	$form['grasp_analysts_map'] = array(
       '#type' => 'select',
       '#title' => t('Select analyst map.'),
       '#options' => $options,
       '#default_value' =>  variable_get('grasp_analysts_map',''),
       '#multiple' => FALSE,
       '#size' => count($options),
       '#description' => t('Select a map to be used as analysts map.'),
   );

   $form['grasp_com_graph_type'] = array(
        '#type' => 'select',
        '#title' => t('Select Component graph type.'),
        '#options' => array('markers'=>'Markers', 'lines' => 'Lines', 'lines+markers' => 'Lines and Markers', 'bar' => 'Bar' ),
        '#default_value' =>  variable_get('grasp_com_graph_type','lines'),
        '#multiple' => FALSE,
        '#size' => 4,
        '#description' => t('Default Component graph type.'),
    );
    $form['grasp_sec_graph_type'] = array(
         '#type' => 'select',
         '#title' => t('Select Sector graph type.'),
         '#options' => array('markers'=>'Markers', 'lines' => 'Lines', 'lines+markers' => 'Lines and Markers', 'bar' => 'Bar'),
         '#default_value' =>  variable_get('grasp_sec_graph_type','bar'),
         '#multiple' => FALSE,
         '#size' => 4,
         '#description' => t('Default Sector graph type.'),
     );

    $form['submit'] = array('#type' => 'submit', '#value' => t('Save configuration'));
    return $form;

}

function grasp_configuration_form_submit($form, &$form_state){
  // drupal_set_message("Custom <pre>".print_r($form_state['values'],TRUE)."</pre>");
  variable_set('grasp_analysts_map',$form_state['values']['grasp_analysts_map']);
  variable_set('grasp_com_graph_type',$form_state['values']['grasp_com_graph_type']);
  variable_set('grasp_sec_graph_type',$form_state['values']['grasp_sec_graph_type']);
}
