/**
 * @file
 * OpenLayers Behavior implementation for clustering.
 */

(function ($) {
/**
 * OpenLayers Cluster Behavior.
 */
Drupal.openlayers.addBehavior('ol_grasp_clustering', function (data, options) {

/**
 * Class: OpenLayers.Strategy.MyAppendableCluster
 * This allows me to addFeatures to my layer without deleting the current ones,
 * also supports toggling activity without clearing the cache so that deactivating
 * the strategy will reapply the features to the layer unclustered, and activating will cluster them again
 *
 * Inherits from:
 *  - <OpenLayers.Strategy.Cluster>
 */
OpenLayers.Strategy.MyAppendableCluster =  OpenLayers.Class(OpenLayers.Strategy.Cluster, {
		distance: options.distance,
     /**
     * the rule to use for comparison
     */
    rule: null,
    /**
     * Method: shouldCluster
     * Determine whether to include a feature in a given cluster.
     *
     * Parameters:
     * cluster - {<OpenLayers.Feature.Vector>} A cluster.
     * feature - {<OpenLayers.Feature.Vector>} A feature.
     *
     * Returns:
     * {Boolean} The feature should be included in the cluster.
     */
    shouldCluster: function(cluster, feature) {
        var superProto = OpenLayers.Strategy.Cluster.prototype;
        return this.rule.evaluate(cluster.cluster[0]) &&
               this.rule.evaluate(feature) &&
               superProto.shouldCluster.apply(this, arguments);
    },

     /**
     * APIMethod: activate
     * Activate the strategy.  Register any listeners, do appropriate setup.
     *
     * Returns:
     * {Boolean} The strategy was successfully activated.
     */
    activate: function() {
        var activated = OpenLayers.Strategy.prototype.activate.call(this);
        if(activated) {
            this.layer.events.on({
                "beforefeaturesadded": this.cacheFeatures,
                "moveend": this.cluster,
                scope: this
            });
        }
        return activated;
    },
     //deactivating clears clusters and adds the features directly
     //DOES NOT CLEARCACHE
     deactivate: function() {
         var deactivated = OpenLayers.Strategy.prototype.deactivate.call(this);
         if(deactivated) {
             this.layer.events.un({ // http://dev.openlayers.org/apidocs/files/OpenLayers/Events-js.html
                 "beforefeaturesadded": this.cacheFeatures,
                 "moveend": this.cluster,
                 scope: this
             });
             this.layer.destroyFeatures(this.layer.features); //<--
             this.layer.addFeatures(this.features); //<--
         }
//         console.log(deactivated);
         return deactivated;
     },


     CLASS_NAME: "OpenLayers.Strategy.MyAppendableCluster"
 });

  var map = data.openlayers;
  var layers = [];
  // For backwards compatiability, if layers is not
  // defined, then include all vector layers
  if (typeof options.layers == 'undefined' || options.layers.length == 0) {
    layers = map.getLayersByClass('OpenLayers.Layer.Vector');
  }
  else {
    for (var i in options.layers) {
      var selectedLayer = map.getLayersBy('drupalID', options.layers[i]);
      if (typeof selectedLayer[0] != 'undefined') {
        layers.push(selectedLayer[0]);
      }
    }
  }

  // if only 1 layer exists, do not add as an array.  Kind of a
  // hack, see https://drupal.org/node/1393460
  if (layers.length == 1) {
    layers = layers[0];
  }

	var BaseCluster = new OpenLayers.Strategy.MyAppendableCluster({
		rule: new OpenLayers.Rule({
    })
	});

	layers.addOptions({ 'strategies': [BaseCluster] });
	BaseCluster.setLayer(layers);//inherit from Strategy.js
	BaseCluster.activate();

  // Cluster chosen layers
    var showLabel = "";
    if (options.display_cluster_numbers) {
      showLabel = "${count}";
    }

		var ctx = {
    	context: {
				myicon: function(feature) {

				  if(feature.cluster.length > 1) { // non accade mai che questa condizione sia verificata
							return './sites/default/files/openlayers_icons/anemometer_mono.png';
				  } else {
//						console.log(feature.cluster[0])
						var s = feature.cluster[0].attributes.field_short_label_value;
						if (s.match(/gs_.*/)) {
							return './sites/default/files/openlayers_icons/GAUGE.svg';
						} else if (s.match(/hp_.*/)){
							return './sites/default/files/openlayers_icons/HPP.svg';
						} else if (s.match(/res_.*/)){
							return './sites/default/files/openlayers_icons/RES.svg';
						} else if (s.match(/NAV.*/)){
							return './sites/default/files/openlayers_icons/NAV.svg';
						} else if (s.match(/ENV.*/)){
							return './sites/default/files/openlayers_icons/ENV.svg';
						} else if (s.match(/FL.*/)){
							return './sites/default/files/openlayers_icons/FLOOD.svg';
						} else if (s.match(/rm_.*/)){
							return './sites/default/files/openlayers_icons/COMP.svg';
						} else if (s.match(/catch_.*/)){
							return './sites/default/files/openlayers_icons/CATCH.svg';
						} else if (s.match(/d_.*/)){
							return './sites/default/files/openlayers_icons/DD.svg';
						} else if (s.match(/HP.*/)){
							return './sites/default/files/openlayers_icons/ENE.svg';
						} else if (s.match(/WS.*/)){
							return './sites/default/files/openlayers_icons/IRRIG.svg';
						} else {
							return './sites/default/files/openlayers_icons/empty.svg';
						}

					}
				},
				mylabel: function(feature){
				var nome = feature.cluster[0].attributes.field_label_value;
					return nome;
				},

				/* ATT with Jquery 1.8 for checkbox checked evaluation use $("#sector_test").prop("checked") instead of $("#sector_test").attr("checked")*/
				shouldDisplay: function(feature){
//						 console.log("setting display to yes");
            var display = "yes"

            if ($("#sector").prop("checked") == true && $("#component").prop("checked") == true){
  						return display;
            } else {

            	if ($("#sector").prop("checked") == false){
				        if (feature.attributes.field_marker_type_value && feature.attributes.field_marker_type_value == "sector") {
				            display = "none";
				        } else if( feature.cluster.length == 1 && feature.cluster[0].attributes.field_marker_type_value == "sector") {
						      // console.log("Should dispaly function of feature");
						      // console.log(feature);
				            display = "none";
				        }
			          return display;
            	}
            	if ($("#component").prop("checked") == false){
				        if (feature.attributes.field_marker_type_value && feature.attributes.field_marker_type_value == "component") {
				            display = "none";
				        } else if( feature.cluster.length == 1 && feature.cluster[0].attributes.field_marker_type_value == "component") {
						      // console.log("Should dispaly function of feature");
						      // console.log(feature);
				            display = "none";
				        }
			          return display;
            	}

            }

        },
			}
		};

    var emptysymbolyzer = {

    };

		// To create a Style object we just pass in a symbolyzer and optionally a context property
		var style = new OpenLayers.Style(emptysymbolyzer, ctx); // funzionante

    var ruleIndividual = new OpenLayers.Rule({
      filter: new OpenLayers.Filter.Comparison({
          type: OpenLayers.Filter.Comparison.LESS_THAN,
          property: "count",
          value: 2,
      }),
			symbolizer :{
				graphicWidth: 42,
        graphicHeight: 50,
        graphicYOffset: -58,
        graphicOpacity: 1,
        externalGraphic: "${myicon}",
        display: "${shouldDisplay}",
        label: "${mylabel}",
            labelOutlineWidth: 4,
            labelOutlineColor : "black",
            fontColor: "#ffffff",
            fontOpacity: 0.8,
            fontSize: "12px"
			},

    });
		// Define three rules to style the cluster features.
    var ruleSmall = new OpenLayers.Rule({
      filter: new OpenLayers.Filter.Comparison({
        type: OpenLayers.Filter.Comparison.BETWEEN,
        property: "count",
        lowerBoundary: 2,
        upperBoundary: options.middle_lower_bound,
      }),
      symbolizer: {
        fillColor: options.low_color,
        strokeColor: options.low_color,
        fillOpacity: 0.8,
        pointRadius: 20,
        label: showLabel,
        labelOutlineWidth: 1,
        fontColor: "#000000",
        fontOpacity: 0.8,
        fontSize: "15px"
      }
    });

		var ruleElse = new OpenLayers.Rule({
      symbolizer: layers.styleMap.styles["default"].defaultStyle,
      elseFilter: true
    });
 	  style.addRules([ruleIndividual, ruleSmall, ruleElse]);

    var styleMap =  new OpenLayers.StyleMap(style);

    layers.styleMap =  styleMap;
		layers.redraw();

		$("#sector").change(function(){
			// From the other examples
			if($(this).attr("checked")){
					if ($("#component").attr("checked")){
						layers.strategies[0].deactivate();
						BaseCluster.rule =  new OpenLayers.Rule({
						});
						BaseCluster.setLayer(layers);//inherit from Strategy.js
						BaseCluster.activate();

						layers.redraw();

					} else {
						console.log("Activate Cluster");
						layers.strategies[0].deactivate();
						BaseCluster.rule =  new OpenLayers.Rule({
								filter: new OpenLayers.Filter.Comparison({
								    type: OpenLayers.Filter.Comparison.EQUAL_TO,
								    property: "field_marker_type_value",
								    value: "sector",
								})
						});
						BaseCluster.setLayer(layers);//inherit from Strategy.js
						BaseCluster.activate();

						layers.redraw();
						// console.log(layers);
					}
			}	else {
						// console.log("Deactivate Cluster");
						layers.strategies[0].deactivate();
						BaseCluster.rule =  new OpenLayers.Rule({
								filter: new OpenLayers.Filter.Comparison({
								    type: OpenLayers.Filter.Comparison.EQUAL_TO,
								    property: "field_marker_type_value",
								    value: "component",
								})
						});
						BaseCluster.setLayer(layers);//inherit from Strategy.js
						BaseCluster.activate();

						layers.redraw();
						// console.log(layers);
				}
			});

			$("#component").change(function(){
			// From the other examples
			if($(this).attr("checked")){
					if ($("#sector").attr("checked")){
						layers.strategies[0].deactivate();
						BaseCluster.rule =  new OpenLayers.Rule({
						});
						BaseCluster.setLayer(layers);//inherit from Strategy.js
						BaseCluster.activate();

						layers.redraw();

					} else {
						// console.log("Activate Cluster");
						layers.strategies[0].deactivate();
						BaseCluster.rule =  new OpenLayers.Rule({
								filter: new OpenLayers.Filter.Comparison({
								    type: OpenLayers.Filter.Comparison.EQUAL_TO,
								    property: "field_marker_type_value",
								    value: "sector",
								})
						});
						BaseCluster.setLayer(layers);//inherit from Strategy.js
						BaseCluster.activate();

						layers.redraw();
						// console.log(layers);
					}
			}	else {
					// console.log("Deactivate Cluster");
					layers.strategies[0].deactivate();
					BaseCluster.rule =  new OpenLayers.Rule({
						  filter: new OpenLayers.Filter.Comparison({
						      type: OpenLayers.Filter.Comparison.EQUAL_TO,
						      property: "field_marker_type_value",
						      value: "sector",
						  })
					});
					BaseCluster.setLayer(layers);//inherit from Strategy.js
					BaseCluster.activate();

					layers.redraw();
					// console.log(layers);
			}
		});

});

})(jQuery);
