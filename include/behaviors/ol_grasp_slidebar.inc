<?php

/**
 * behavior
 */
class ol_grasp_slidebar extends openlayers_behavior {
/**
   * Provide initial values for options.
   */
  function options_init() {
    return array(
      'graspsearchfromslider' => '',
    );
  }

  /**
   * Render.
   */
  function render(&$map) {
		plotly_js_load();
		drupal_add_library('system', 'jquery.cookie');
    drupal_add_library('system', 'effects.slide'); // slider effect this is the one ported with jquery update
    drupal_add_js(drupal_get_path('module', 'grasp') . '/js/graspSectorMW.js');
    drupal_add_js(drupal_get_path('module', 'istsos') . '/js/istsosutils.js');
    drupal_add_js(drupal_get_path('module', 'grasp') . '/js/graspD3TreeGraph.js');
    drupal_add_js(drupal_get_path('module', 'grasp') . '/include/behaviors/ol_grasp_slidebar.js');
    return $this->options;
  }
}
