/**
 * @file
 * JS Implementation of OpenLayers behavior.
 */

/**
 * Slidebar behavior.
 * We use a namespaced Richshaw graph for Indicators in order to call it from openlayers_behavior_immrpopupcluster.js
 */

(function ($) {
	// Drupal.behaviors.graspD3TreeGraph = Drupal.behaviors.graspD3TreeGraph || {};
	/**/
	/* OpenLayers behavior get map data */
	Drupal.openlayers.addBehavior('ol_grasp_slidebar', function (data, options) {

		// console.log(data);
		// console.log(options);
		var epsg = data.map.projection;
		// console.log(epsg);
		/********************************************************************************/
		/* MAIN START */
		/* Global variables */
		/********************************************************************************/
		var map = data.openlayers;

		// Drupal.behaviors.graspSidebarSlider = Drupal.behaviors.graspSidebarSlider || {};
		// console.log(Drupal.behaviors.graspSidebarSlider);
		$( "#trigger" ).on( "click", function() {
			// jQuery UI toggle
			$( "#panel1" ).toggle( 'slide', {}, 200 );
		});

/***************************************************/
/*                       SEARCH                   */
/***************************************************/
		$("#search_submit").click(function(event){
			var keys = $('#search').val();
			$("#search_results").empty();
			// console.log(keys);
			// console.log(map);
			$.ajax({
					url: "?q=grasp_slidebar_search",
					type: "POST",
					data: {keys: keys, projection: epsg},
					dataType : "json",
					success: successFunction,
			});
			function successFunction (data, textStatus, jqXHR) {
				if (data.length > 0){
					$.each( data, function (k,v){
						console.log(v);
						$("#search_results").append('<li><a class="zoomto" lat='+v.lat+' lon='+v.lon+' href="#">'+v.title+'</a></li>');
					});
					$('.zoomto').click(function(){
							var lon = $(this).attr('lon');
							var lat = $(this).attr('lat');
						 	var pointCenter = new OpenLayers.LonLat(lon,lat);
						 	map.setCenter(pointCenter, 15);
					});
				} else {
					$("#search_results").append('<li>No results fount for your search.</li>');
				}
			}
		});

/***************************************************/
/*             Project Hierarchy                   */
/***************************************************/

		/* The root projectTree is fixed once opened */
		$('#ProjectTree_MW').on('show.bs.modal', function (e) {
			if (!$('#PHTree > svg').length){
				Drupal.behaviors.graspD3TreeGraph.D3TreeProjectGraph('root', '#PHTree');
			}
		});
		// Back link to Project Hierarchy
		$('#BTStep1').click(function () {
			// close the Sector modal window
			$('#Sector_MW').modal('hide');
			// open the Project Hierarchy modal window
			$('#ProjectTree_MW').modal('show');
		});
		// Back link from SSector_MW to Sector_MW
		$('#BTStep2').click( function () {
			console.log('BTStep2 function click');
			// close the SubSector modal window
			$('#SSector_MW').modal('hide');
			// Prepare the Sector Modal Window
			Drupal.behaviors.graspSectorMW.SectorMWPrepare()
			// Call the tree graph function for the clicked sector
			Drupal.behaviors.graspD3TreeGraph.D3TreeProjectGraph(Drupal.behaviors.graspSectorMW.SectorNid, '#SHTree');
			// get a list of marker attached to this sector
			Drupal.behaviors.graspSectorMW.SectorMWFill(Drupal.behaviors.graspSectorMW.SectorNid)
			// show the window
			$('#Sector_MW').modal('show');
    });
		// Back link from Indicator_MW to SSector_MW
		$('#BTStep3').click( function () {
			console.log('BTStep3 function click');
			// close the Indicator modal window
			$('#Indicator_MW').modal('hide');
			// Prepare the SSector Modal Window
			Drupal.behaviors.graspSectorMW.SSectorMWPrepare()
			// Call the tree graph function for the clicked sector
			Drupal.behaviors.graspD3TreeGraph.D3TreeProjectGraph(Drupal.behaviors.graspSectorMW.SectorNid, '#SSHTree');
			// Fill the sub sector modal window
			Drupal.behaviors.graspSectorMW.SSectorMWFill();
			$('#SSector_MW').modal('show');
    });

/**/
/* Add functionality to Istsos Services selection radio buttons */
/**/
		$("input[name=istsos_service]:radio").click(function(event){
			var now = new Date();
			now.setFullYear(now.getFullYear()+1);
			// Set cookie with Drupal format
			$.cookie('Drupal.visitor.istsosscheme', $(this).val(), {path: "/", expires: now});
			$.cookie('Drupal.visitor.istsosdescr', $(this).attr('description'), {path: "/", expires: now});
		});

	});
})(jQuery);
