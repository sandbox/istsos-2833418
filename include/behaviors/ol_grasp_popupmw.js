/**
 * @file
 * JS Implementation of OpenLayers behavior.
 */

/**
 * Javascript Drupal Theming function for inside of Popups
 *
 * To override
 *
 * @param feature
 *  OpenLayers feature object.
 * @return
 *  Formatted HTML.
 */
(function ($) {
/* Define the namespace for the popup inside openlayers */
// Make sure the namespace exists
Drupal.openlayers.popup = Drupal.openlayers.popup || {};
/* Define a namespace for exchange variables between behaviors */
// Drupal.behaviors.imrrSectors = Drupal.behaviors.imrrSectors || {};
/**
 * OpenLayers Popup Behavior for GRASP project
 */
Drupal.openlayers.addBehavior('ol_grasp_popupmw', function (data, options) {
  var map = data.openlayers;
  var layers = [];
  // var selectedFeature;

  // For backwards compatiability, if layers is not
  // defined, then include all vector layers
  if (typeof options.layers == 'undefined' || options.layers.length == 0) {
    layers = map.getLayersByClass('OpenLayers.Layer.Vector');
  }
  else {
    for (var i in options.layers) {
      var selectedLayer = map.getLayersBy('drupalID', options.layers[i]);
      if (typeof selectedLayer[0] != 'undefined') {
        layers.push(selectedLayer[0]);
      }
    }
  }

  // if only 1 layer exists, do not add as an array.  Kind of a
  // hack, see https://drupal.org/node/1393460
  if (layers.length == 1) {
    layers = layers[0];
  }

  var popupSelect = new OpenLayers.Control.SelectFeature(layers,
    {
      onSelect: function(feature) {
      	if(feature.cluster.length > 1){	 // If the feature is a cluster longer than one
					var cluster_bounds=new OpenLayers.Bounds();
					feature.cluster.forEach(function(feature){	// get the bounds for the features
  				  cluster_bounds.extend(feature.geometry);
					});
					var pointCenter = cluster_bounds.getCenterLonLat();
					var scaled_bound = cluster_bounds.scale( (1/feature.cluster.length)*5 ,pointCenter);
					map.zoomToExtent(scaled_bound); // zoom the map to the bounds
	      } else {
					/************************************************************************************/
					var nome = feature.cluster[0].attributes.name;
					var title = feature.cluster[0].attributes.field_description_short_value;
					subSectorName = feature.cluster[0].attributes.field_description_short_value;
					/**********************************************************************/
					/*                             SECTORS                                */
					/**********************************************************************/
						// Verify if the clicked feature is a sector
						if (feature.cluster[0].attributes.field_marker_type_value == "sector"){
                console.log(feature);
								Drupal.behaviors.graspSectorMW.FOINid = feature.cluster[0].attributes.nid;
                Drupal.behaviors.graspSectorMW.FOIName = feature.cluster[0].attributes.name;
                // call function sector_marker in sector.inc
                // we get back the
                // sector_nid - node id of the sector this marker is attched to
                // sector_name  - the name of the sector to set the modal window title
                // selected_marker - the name of the selected marker // TODO is this used????
                // markers_names - a list of markers attached to this sector
								$.ajax({
										url: "?q=grasp_sector_marker",
										type: "POST",
										data: {foinid: Drupal.behaviors.graspSectorMW.FOINid},
										dataType:'json',
										async: false,
										success: setSectorNid,
								});

								function setSectorNid (data, textStatus, jqXHR) {
                  var pjd = $.parseJSON(data)
                  if (pjd.response == 'success'){
                    console.log(pjd);
                    Drupal.behaviors.graspSectorMW.SectorNid = pjd.sector_nid;
                    Drupal.behaviors.graspSectorMW.SectorName = pjd.sector_name;
                    Drupal.behaviors.graspSectorMW.FOIList = pjd.markers_names;
                    // Prepare the SSector Modal Window
                    Drupal.behaviors.graspSectorMW.SSectorMWPrepare();
                    // Call the tree graph function for the clicked sub sector
                    Drupal.behaviors.graspD3TreeGraph.D3TreeProjectGraph(Drupal.behaviors.graspSectorMW.SectorNid, '#SSHTree');
                    // Fill the sub sector modal window
                    Drupal.behaviors.graspSectorMW.SSectorMWFill();
                  } else {
                    alert(pjd.message)
                  }
								}
						}

						/**********************************************************************/
						/*                          COMPONENTS                                */
						/**********************************************************************/
						// Verify if the clicked feature is a component
						if (feature.cluster[0].attributes.field_marker_type_value == "component"){
                console.log(feature);
								Drupal.behaviors.graspSectorMW.FOINid = feature.cluster[0].attributes.nid;
								// var FOIName = feature.cluster[0].attributes.field_short_label_value; // TODO old approac with short label
                Drupal.behaviors.graspSectorMW.FOIName = feature.cluster[0].attributes.name;
								/* get the procedures attached to this marker/FOI */
								$.ajax({
										url: "?q=grasp_component_procedures", // function component_procedures in component.inc
										type: "POST",
										data: {foiname: Drupal.behaviors.graspSectorMW.FOIName, foinid: Drupal.behaviors.graspSectorMW.FOINid},
										dataType:'json',
										success: procedureRadio,
										complete: compToggleThrobber,
								});

								function procedureRadio (data, textStatus, jqXHR) {
                  // Prepare the Component modal window
                  Drupal.behaviors.graspSectorMW.ComponentMWPrepare();
                  //
									var pjd = $.parseJSON(data);
									console.log(pjd);

                  // Get the marker/FOI short and long descriptions to fill the accordion
                  $('#compDesc').html(pjd.shdescription);
                  $('#compLongDesc').html(pjd.description);

                  if (pjd.procedures) {
                    // Fill the procedures radiobuttons
                    $.each(pjd.procedures, function (namePrc, value) {
  									 $('#comp_prc_list').append('<input type="radio" name="proc_list" frequency="'+value.frequency+'" value="'+namePrc+'" class="procedure"/> '+namePrc+' - '+value.description+' - '+value.frequency+'<span class="glyphicon glyphicon-refresh spinning"></span><br />');
  									});
                    // Add procedures radiobuttons functionalities
                    // TODO quando seleziono la procedure carico le sue alternatives nel multiselect widget
                    $('.procedure').on('change', function() {
                      Drupal.behaviors.graspSectorMW.ComponentMWFrequenciesClean();
                      Drupal.behaviors.graspSectorMW.PRCName = $('input[name=proc_list]:checked').val();
                      Drupal.behaviors.graspSectorMW.PRCOffering = $('input[name=proc_list]:checked').attr('frequency');
                      Drupal.behaviors.graspSectorMW.PlotlyMWFill('Component');
                    });
                  } else {
                    $('#comp_prc_list').html('No procedures found for this component.');
                  }
                  // Show the Component modal window
                  $('#Component_MW').modal('show');
								}

  							function compToggleThrobber(){
  								{ $('.glyphicon.spinning').hide();
                    // $('.ajax-progress').hide();
                   }
  							}
						}
/****************************************************************************/
	      }
      },
      onUnselect: function(feature) {
        this.unselectAll();
      }
    }
  );

  map.addControl(popupSelect);
  popupSelect.activate();
  Drupal.openlayers.popup = popupSelect;
});

})(jQuery);
