#!/usr/bin/env python

# -*- coding: utf-8 -*-
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

'''
This script can be used to import measurement from a CSV file.
'''

try:
    import os
    os.environ['PYTHON_EGG_CACHE'] = '/tmp'

    import traceback
    import json
    import pprint
    from pprint import pprint
    import glob
    import csv
    import ConfigParser
    import psycopg2
    import sys
    sys.path.append('/usr/local/istsos')
    import lib.requests as requests
    import lib.argparse as argparse
    from lib import isodate as iso
    from datetime import datetime, timedelta
    import logging
    import logging.handlers

except ImportError as e:
    raise e

# print ("Scriptname: %s" % str(sys.argv[0]))
# print ("Source directory: %s" % str(sys.argv[1]))

#
# Configuration varibles
#

url = sys.argv[2]  # -- Global variable to define the istsos installation url
# credentials = {'dbname' : 'vietnam', 'user' : 'vietnam', 'host' : 'localhost', 'password' : 'v13tn4m'} # -- Set database credentials to connect to Drupal database
# credentials = eval(sys.argv[1])
credentials = {'dbname': sys.argv[5], 'user': sys.argv[3], 'host': sys.argv[6], 'password': sys.argv[4]}  # -- Set database credentials to connect to Drupal database
logPath = '/usr/local/istsos/logs/websingleimport.log'  # -- the path to the error log file for this importer
file_name = sys.argv[1]  # -- the path to the uploaded file

# Set up a specific logger with our desired output level
formatter = logging.Formatter('%(asctime)-6s: %(name)s - %(levelname)s - %(message)s')
logger = logging.getLogger('massive_import')
handler = logging.handlers.RotatingFileHandler(filename=logPath, maxBytes=1024*1024, backupCount=20)
handler.setFormatter(formatter)
logger.addHandler(handler)
logger.setLevel(logging.DEBUG)

req = requests.session()
user = ''
passw = ''

# verify the databse connection
try:
    conn = psycopg2.connect(**credentials)
    cur = conn.cursor()

except psycopg2.DatabaseError, e:
    print '-ERROR- %s' % e
    logger.error('Error %s', e)
    sys.exit(1)

# Get a list of available schemas inside the given database
services = []
sql = "select catalog_name,schema_name from information_schema.schemata"
try:
    cur.execute(sql)
except Exception as err:
    # print "errore query"
    logger.warning('Query exception on sql %s', sql)
    print '-ERROR- Query exception on sql %s' % sql
    logger.warning(err)
    logger.info('Cannot get list of schema for given database')

rows = cur.fetchall()
if rows:
    # pprint (rows)
    for pos in range(0, len(rows)):
        # pprint(rows[pos][1])
        services.append(rows[pos][1])

#
# DEFINITION OF FUNCTIONS
#


def verifycreate_environment(unit, frequency, ob_prop, service):
    # print "environtment check"
    # Load the configuration section for this service to obtain the observed properties urn
    if service in services:
        configsections = req.get("%s/wa/istsos/services/%s/configsections" % (url, service), auth=(user, passw), verify=False)
        # cfres = configsections.json
        cfres = configsections.json()
        # print "cfres %s" % (cfres)
        if cfres['success'] is False:
            # print 'Failed to get configuration information'
            logger.warning('Failed to get configuration information')
            print "-ERROR- Failed to get configuration information."
            return False
        else:
            urn = cfres['data']['urn']['parameter']
            webheader.append(service)

        # UNIT OF MEASURE
        uom = req.get("%s/wa/istsos/services/%s/uoms" % (url, service), auth=(user, passw), verify=False)
        uomres = uom.json()
        if uomres['success'] is False:
            # print 'Failed get uom list'
            logger.warning('Failed to get unit of measure list')
            print "-ERROR- Failed to get unit of measure list."
            return False
        else:
            uomarr = []
            for pos in range(0, len(uomres['data'])):
                uomarr.append(uomres['data'][pos]['name'])
            if unit not in uomarr:
                # Create the the new unit of measure
                res = req.post("%s/wa/istsos/services/%s/uoms" % (
                    url,
                    service),
                    # prefetch=True,
                    auth=(user, passw),
                    verify=False,
                    data=json.dumps({
                        "name": unit,
                        "description": '',
                        })
                        )
                response = res.json()
                if response['success'] is False:
                    logger.warning('Failed to insert unit of measure %s', unit)
                    print "-ERROR- Failed to insert unit of measure %s." % unit
                    return False

        # OFFERINGS
        offe = req.get("%s/wa/istsos/services/%s/offerings/operations/getlist" % (url, service), auth=(user, passw), verify=False)
        offeres = offe.json()
        if offeres['success'] is False:
            # print 'Failed get offerings list'
            logger.warning('Failed get offerings list')
            print "-ERROR- Failed get offerings list."
            return False
        else:
            offearr = []
            for pos in range(0, len(offeres['data'])):
                offearr.append(offeres['data'][pos]['name'])

            if frequency not in offearr:
                # Create the the new offering
                res = req.post("%s/wa/istsos/services/%s/offerings" % (
                    url,
                    service),
                    # prefetch=True,
                    auth=(user, passw),
                    verify=False,
                    data=json.dumps({
                        "name": frequency,
                        "description": '',
                        "expiration": '',
                        "active": 'true'
                        })
                        )

                response = res.json()
                if response['success'] is False:
                    logger.warning('Failed to insert offering %s', frequency)
                    print "-ERROR- Failed to insert offering %s." % frequency
                    return False

        # OBSERVED PROPERTIES
        obsp = req.get("%s/wa/istsos/services/%s/observedproperties" % (url, service), auth=(user, passw), verify=False)
        obspres = obsp.json()
        if obspres['success'] is False:
            # print 'Failed get observed properties list'
            logger.warning('Failed get observed properties list')
            print "-ERROR- Failed get observed properties list."
            return False
        else:
            obsparr = []
            for pos in range(0, len(obspres['data'])):
                # pprint(obspres['data'][pos]['name'])
                obsparr.append(obspres['data'][pos]['name'])

            ob_proparr = []
            for k in range(0, len(ob_prop)):
                ob_proparr.append(ob_prop[k]+':'+ob_prop[k])

            obspmiss = list(set(ob_proparr) - set(obsparr))
            if obspmiss:
                # print "Missed observed properties"
                # pprint(obspmiss)
                for k in range(0, len(obspmiss)):
                    # Create the the new observed propety
                    res = req.post("%s/wa/istsos/services/%s/observedproperties" % (
                        url,
                        service),
                        # prefetch=True,
                        auth=(user, passw),
                        verify=False,
                        data=json.dumps({
                            "name": obspmiss[k],
                            "definition": urn+obspmiss[k],
                            "description": '',
                            "constraint": ''
                            })
                            )

                    response = res.json()
                    if response['success'] is False:
                        logger.warning('Failed to insert observed properties')
                        print "-ERROR- Failed to insert observed properties."
                        return False
        return True
    else:
        logger.error('Service %s is not in the list of schema for the configured database', service)
        print "-ERROR- Service %s is not inside registered services." % service
        return False


def verifycreate_procedure(proc, marker_id, description, ob_prop, service, unit):
    # print "procedure check"
    # Load procedure description
    # print "GET: %s/wa/istsos/services/%s/procedures/%s" % (url,service,proc)
    res = req.get("%s/wa/istsos/services/%s/procedures/%s" % (url, service, proc), auth=(user, passw), verify=False)
    response = res.json()
    # pprint(response)
    if response['success'] is False:
        # print "Creating procedure"
        logger.info('Start creation of new procedure %s', proc)
        # Load the configuration section for this service to obtain the observed properties urn
        configsections = req.get("%s/wa/istsos/services/%s/configsections" % (url, service), auth=(user, passw), verify=False)
        cfres = configsections.json()
        # print "cfres %s" % (cfres)
        if cfres['success'] is False:
            # print 'Failed to get configuration information'
            logger.warning('Failed to get configuration information')
            return False
        else:
            # pprint(cfres)
            urn = cfres['data']['urn']['parameter']

        # Create the procedure inside the service
        sql = "SELECT ST_X(field_marker_point_geometry), ST_Y(field_marker_point_geometry), ST_SRID(field_marker_point_geometry) FROM field_data_field_marker_point WHERE entity_id ="
        sql += " (SELECT entity_id FROM field_data_field_short_label WHERE bundle='marker' AND field_short_label_value='%s')" % (marker_id)
        try:
            cur.execute(sql)
        except Exception as err:
            # print "errore query"
            logger.warning('Query exception on sql %s', sql)
            logger.warning(err)
            logger.info('Verify the marker with short label %s exist', marker_id)
            print "-ERROR- Verify the marker with short label %s exist." % marker_id

        rows = cur.fetchall()
        if rows:
            # print type(rows)
            for coords in rows:
                # print type(row)
                x = coords[0]
                y = coords[1]
                z = coords[2]

            data = {}
            data['system_id'] = proc
            data['system'] = proc
            data['description'] = description
            data['keywords'] = ''
            data['identification'] = []
            data['classification'] = [
                {"name": "System Type", "definition": "urn:ogc:def:classifier:x-istsos:1.0:systemType", "value": "insitu-fixed-point"},
                {"name": "Sensor Type", "definition": "urn:ogc:def:classifier:x-istsos:1.0:sensorType", "value": "-"},
                ]
            data['characteristics'] = ""
            data['contacts'] = []
            data['documentation'] = []
            data['location'] = {
                "type": "Feature",
                "geometry": {
                    "type": "Point",
                    "coordinates": [str(y), str(x), '100'],
                    },
                "crs": {
                    "type": "name",
                    "properties": {"name": str(z)},
                    },
                "properties": {"name": marker_id}
                # "properties" => array("name" => 'title') //  insert the node title that is the FOI name
                }
            data['interfaces'] = ""
            data['inputs'] = []
            data['history'] = []
            data['capabilities'] = []

            outputs = []
            time = {'name': 'Time', 'definition': 'urn:ogc:def:parameter:x-istsos:1.0:time:iso8601', 'uom': 'iso8601', 'description': ''}
            outputs.append(time)
            for k in range(0, len(ob_prop)):
                out = {'name': ob_prop[k]+':'+ob_prop[k], 'definition': urn+ob_prop[k]+':'+ob_prop[k], 'uom': unit, 'description': ''}
                outputs.append(out)

            data['outputs'] = outputs
            # pprint(data)
            res = req.post("%s/wa/istsos/services/%s/procedures" % (
                url,
                service),
                # prefetch=True,
                auth=(user, passw),
                verify=False,
                data=json.dumps(
                    data
                    )
                )

            # read response
            # pprint(res.json)
            if res.json()['success'] is False:
                # print 'Failed to create procedure %s' % (proc)
                logger.warning('Failed to create procedure %s', proc)
                print "-ERROR- Failed to create procedure %s." % proc
                return False
            else:
                logger.info('Procedure %s created correctly', proc)
                webheader.append(proc)
                return True
        else:
            logger.warning('Failed to create procedure %s - Verify the marker with short label %s exist', proc, marker_id)
            print "-ERROR- Failed to create procedure %s - Verify the marker with short label %s exist." % (proc, marker_id)
    else:
        # print "The procedure exist"
        webheader.append(proc)
        return True
    return True


def moveprcto_offering(proc, frequency, service):
    # print "move the proc to offering"
    prcmemb = req.get("%s/wa/istsos/services/%s/offerings/%s/procedures/operations/memberslist" % (url, service, frequency), auth=(user, passw), verify=False)
    prcmembres = prcmemb.json()
    if prcmembres['success'] is False:
        # print 'Failed get offering list'
        logger.warning('Failed get offering list')
        print "-ERROR- Failed get offering list."
        return False
    else:
        # pprint(prcmembres)
        prcmembarr = []
        for pos in range(0, len(prcmembres['data'])):
            prcmembarr.append(prcmembres['data'][pos]['name'])

        if proc not in prcmembarr:
            data = []
            out = {'offering': frequency, 'procedure': proc}
            data.append(out)
            # Insert the procedure inside the offering
            res = req.post("%s/wa/istsos/services/%s/offerings/%s/procedures" % (
                url,
                service,
                frequency),
                # prefetch=True,
                auth=(user, passw),
                verify=False,
                data=json.dumps(
                    data
                    )
            )
            # pprint(res.json)
            if res.json()['success'] is False:
                # print 'Failed to move procedure %s inside offering %s' % (proc, frequency)
                logger.warning('Failed to move procedure %s inside offering %s', proc, frequency)
                print '-ERROR- Failed to move procedure %s inside offering %s' % (proc, frequency)
                return False
    return True


#
# ---------- MAIN
#

# print " -------------------- Processing file %s ------------------------------" % (file_name)
logger.info(' -------------- Start processing file %s ----------------', file_name)
parse = True
optest = True
header = {}
obsprop = []
data = []
sem = 0
webheader = []
webheader.append('chartdata')
with open(file_name, 'rb') as csvdata:
    csvdata = csv.reader(csvdata, delimiter=',')
    for row in csvdata:
        if row[0] == 'name':
            header['proc'] = row[1]
        if row[0] == 'id_type':
            header['id_type'] = row[1]
        if row[0] == 'marker_id':
            header['marker_id'] = row[1]
        if row[0] == 'description':
            header['description'] = row[1]
        if row[0] == 'unit':
            header['unit'] = row[1]
        if row[0] == 'frequency':
            header['frequency'] = row[1]
        if row[0] == 'scenario':
            header['service'] = row[1]
        if row[0] == 'timestart' and row[1] == 'timeend':
            row.pop(0)
            row.pop(0)
            sem = 1
            # obsprop.append(row)
            obsprop = row
            header['obsprop'] = row
        if row[0] == 'timestart' and row[1] != 'timeend':
            # advance one step if the row[1] is timeend
            row.pop(0)
            # obsprop.append(row)
            obsprop = row
            header['obsprop'] = row
        if row[0].isdigit() and sem == 1:
            row.pop(1)
            data.append(row)
        if row[0].isdigit() and sem == 0:
            data.append(row)

    # print "----------- VERIFY HEADER--------------"
    if 'proc' not in header:
        print "-ERROR- Missing procedure name in csv"
        logger.warning('Missing procedure name - skipping csv file %s', file_name)
        parse = False
    if 'id_type' not in header:
        print "-ERROR- Missing id_type in csv"
        logger.warning('Missing id_type - skipping csv file %s', file_name)
        parse = False
    if 'marker_id' not in header:
        print "-ERROR- Missing marker_id in csv"
        logger.warning('Missing marker_id - skipping csv file %s', file_name)
        parse = False
    if 'description' not in header:
        print "-ERROR- Missing description in csv"
        logger.warning('Missing description - skipping csv file %s', file_name)
        parse = False
    if 'frequency' not in header:
        print "-ERROR- Missing frequency in csv"
        logger.warning('Missing frequency - skipping csv file %s', file_name)
        parse = False
    if 'service' not in header:
        print "-ERROR- Missing service/scenario in csv"
        logger.warning('Missing service/scenario - skipping csv file %s', file_name)
        parse = False
    if 'obsprop' not in header:
        print "-ERROR- Missing observed properties in csv"
        logger.warning('Missing observed properties - skipping csv file %s', file_name)
        parse = False
    if 'unit' not in header:
        print "-ERROR- Missing unit of measure in csv"
        logger.warning('Missing unit of measure - skipping csv file %s', file_name)
        parse = False

    if parse:
        # print "calling environment check"
        ve = verifycreate_environment(header['unit'], header['frequency'], header['obsprop'], header['service'])
        if not ve:
            logger.warning('Failed to verify environment for file %s - on service %s', file_name, header['service'])
            print "-ERROR- Failed to verify environment for file %s - on service %s." % (file_name, header['service'])
            sys.exit()

        vp = verifycreate_procedure(header['proc'], header['marker_id'], header['description'], header['obsprop'], header['service'], header['unit'])
        if not vp:
            logger.warning('Failed to verify procedure for file %s - on service %s - procedure name %s', file_name, header['service'], header['proc'])
            print "-ERROR- Failed to verify procedure for file %s - on service %s - procedure name %s." % (ffile_name, header['service'], header['proc'])
            sys.exit()

        mo = moveprcto_offering(header['proc'], header['frequency'], header['service'])
        if not mo:
            logger.warning('Failed to move procedure for file %s - procedure name %s to offering %s', file_name, header['proc'], header['frequency'])
            print '-ERROR- Failed to move procedure for file %s - procedure name %s to offering %s.' % (file_name, header['proc'], header['frequency'])
            sys.exit()


        if ve and vp and mo:
            # print "check passed"
            # Load procedure description line 148 csv2istsos.py
            res = req.get("%s/wa/istsos/services/%s/procedures/%s" % (
                url,
                header['service'],
                header['proc']),
                auth=(user, passw), verify=False)

            response = res.json()
            # pprint(response['data']['outputs'])

            if response['success'] is False:
                # print "FAILED"
                logger.warning('Description of procedure %s on service %s failed', header['proc'], header['service'])
                # Create the procedure inside the service
            else:
                # print "OK"
                aid = response['data']['assignedSensorId']
                # The procedure exist fill with data

                # TODO ORIG 1 Load the configuration section for this service
                # to obtain the observed properties urn
                # Utilizzato per creare un array di nomi di observed properties completo
                # nei file che passiamo abbiamo solo la parte finale
                configsections = req.get("%s/wa/istsos/services/%s/configsections" % (url,header['service']), auth=(user, passw), verify=False)
                conf = configsections.json()
                # pprint(response['data']['urn']['parameter'])
                urn = conf['data']['urn']['parameter']

                obsindex = {}
                # Create a full observation name composed with the urn + observed property name
                for i in range(0, len(header['obsprop'])):
                    obsindex[urn + obsprop[i] + ':' + obsprop[i]] = i+1

                # Getting observed properties from describeSensor response
                op = []
                for out in response['data']['outputs']:
                    if not ':qualityIndex' in out['definition']:
                        op.append(out['definition'])

                # Load of a getobservation request if we use the TODO ORIG 1
                # res = req.get(
                #     "%s/wa/istsos/services/%s/operations/getobservation/"
                #     "offerings/%s/procedures/%s/observedproperties/%s/ev"
                #     "enttime/last" % (
                #         url, header['service'], 'temporary', header['proc'], urn),
                #     auth=(user, passw),
                #     verify=False)

                # Load of a getobservation request - line 174 csv2istsos.py
                res = req.get(
                    "%s/wa/istsos/services/%s/operations/getobservation/"
                    "offerings/%s/procedures/%s/observedproperties/%s/ev"
                    "enttime/last" % (
                        url, header['service'], 'temporary', header['proc'], ','.join(op)),
                    auth=(user, passw),
                    verify=False)

                response = res.json()

                if response['success'] is False:
                    logger.warning('Last observation of procedure %s for service %s failed', header['proc'], header['service'])
                    print '-ERROR- Last observation of procedure %s for service %s failed.' % (header['proc'], header['service'])

                else:
                    # print "get observation last suceed"
                    # pprint (response)

                    topost = response['data'][0]
                    topost['AssignedSensorId'] = aid

                    # # Set values array empty, can contain 1 value if
                    # # procedure not empty
                    # if len(topost['result']['DataArray']['values']) == 1:
                    #     lastMeasure = topost['result']['DataArray']['values'][0]
                    # else:
                    #     lastMeasure = None

                    topost['result']['DataArray']['values'] = []

                    # discover json observed property disposition
                    jsonindex = {}
                    for pos in range(0, len(topost['result']['DataArray']['field'])):
                        field = topost['result']['DataArray']['field'][pos]
                        # print "pos %s field %s" % (pos, field)
                        # if not ':qualityIndex' in field['definition']:
                        jsonindex[field['definition']] = pos

                    # pprint (len(jsonindex))

                    # Check if all the observedProperties of the procedure are included in the CSV file (quality index is optional)
                    # print "------------ Json index --------------------"
                    for k, v in jsonindex.iteritems():
                        # print "value %s index %s" % (k, v)
                        # value urn:ogc:def:parameter:x-istsos:1.0:time:iso8601 index 0
                        if k in obsindex:
                            # pprint(obsindex[k])
                            continue
                        elif ':qualityIndex' in k:
                            continue
                        elif ':iso8601' in k:
                            continue
                        else:
                            logger.warning('Mandatory observed property %s is not present in the CSV.', k)
                            print "-ERROR- Mandatory observed property %s is not present in the CSV." % k
                            optest = False

                    if optest:
                        # Create the array structure to insert
                        begin = None
                        end = None
                        # loop data array 1 row for cicle
                        datarows = len(data)
                        count = 0

                    for values in data:
                        # pprint (values)
                        # pprint (len(jsonindex))
                        observation = ['']*len(jsonindex)
                        for k, v in jsonindex.iteritems():
                            # print "value %s index %s" % (k, v)

                            webval = []
                            if v == 0:
                                # time value to be converted in unix timestamp and isotime
                                days_before_epoch = 719529-int(values[0])
                                unixDate = -days_before_epoch * 86400
                                #2013-01-01T00:10:00.000000+0100
                                val = datetime.fromtimestamp(unixDate).strftime('%Y-%m-%dT%H:%M:%S.000000+0100')
                            if k in obsindex:
                                val = values[obsindex[k]]
                            elif ':qualityIndex' in k:
                                val = '100'  # quality inde

                            observation[v] = val

                        # get first date
                        if count == 0:
                            begin = observation[0]
                            webheader.append(begin.split('T')[0])

                        # get last date
                        if count == (datarows-1):
                            end = observation[0]
                            webheader.append(end.split('T')[0])

                        # attach to object
                        topost['result']['DataArray']['values'].append(observation)
                        count += 1

                    # Verify if begin and end position are not equal
                    # This can be true for one day measure
                    if (begin == end):
                        date = datetime.strptime(end, '%Y-%m-%dT%H:%M:%S.000000+0100')
                        end = date + timedelta(days=1)
                        val = end.strftime('%Y-%m-%dT%H:%M:%S.000000+0100')
                        topost["samplingTime"] = {
                            "beginPosition": begin,
                            "endPosition": val
                        }
                    else:
                        topost["samplingTime"] = {
                            "beginPosition": begin,
                            "endPosition": end
                        }


                    # pprint("Data to post")
                    # pprint(topost["samplingTime"])

                    res = req.post("%s/wa/istsos/services/%s/operations/insertobservation" % (
                        url,
                        header['service']),
                        # prefetch=True,
                        auth=(user, passw),
                        verify=False,
                        data=json.dumps({
                            "ForceInsert": "true",
                            "AssignedSensorId": aid,
                            "Observation": topost
                        })
                    )
                    # pprint(res.json)
                    response = res.json()
                    if response['success'] is False:
                        logger.warning('Failed to insert observation values inside procedure %s', header['proc'])
                        print "-ERROR- Failed to insert observation values inside procedure %s." % header['proc']
                    else:
                        logger.info('Insert observation values inside procedure %s correctly executed.', header['proc'])
                        # print(webheader)
                        print "-".join(map(str, ["webheader", webheader]))
                        if header['id_type'].lower() == 'i':
                            print "INDICATOR-%s,%s" % (header['proc'], header['description'])
