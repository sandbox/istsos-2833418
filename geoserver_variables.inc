<?php

function geoserver_varaibles(){
	  return drupal_get_form("geoserver_varaibles_form");
}

function geoserver_varaibles_form($form,&$form_state){
  $url        = variable_get('geoserver_url', '');
  $workspace  = variable_get('geoserver_workspace', '');
  $workspaces = array('');
  $workspace_type = 'select';

  if (!empty($url)) {
    try {
      $result = geoserver_get('rest/workspaces.json');
#        	drupal_set_message("geoserver_ui.admin.inc line 25 <pre>".print_r($result,TRUE)."</pre>");
      // Only use data if it is an array to take account for GeoServer sending an empty string instead of an empty array
      if (is_array($result->data->workspaces->workspace)) {
        foreach ($result->data->workspaces->workspace as $ws) {
          $workspaces[$ws->name] = $ws->name;
        }
      }
    } catch (geoserver_resource_exception $exc) {
      $workspace_type = 'textfield';
      drupal_set_message(t('Failed to get list of workspaces from GeoServer. See the log for details.'), 'error');
      watchdog('geoserver', 'Failed to get list of workspaces from GeoServer: @exception',
        array('@exception' => $exc->getMessage()), WATCHDOG_ERROR);
    }
  }

	$form['instr'] = array(
		'#type' => 'fieldset',
		'#title' => t('Instructions'),
#		'#weight' => 5,
		'#collapsible' => TRUE,
		'#collapsed' => TRUE,
	);

  $form['instr']['description'] = array(
	'#prefix' => '<div>',
	'#suffix' => '</div>',
	'#markup' => '<p>For a new installation this page make it possible to reset the GeoServer variables in order to connect to another GeoServer instance.</p>
                <p>Use the Reset button in order to empty all the varibles</p>
                <p>Insert the correct GeoServer url and then Save the configuration</p>
                <p>Logout an login again in order to authenticate to the new GeoServer url</p>
                <p>If you are correctly authenticated to geoserver the GeoServer Workspace will show a list of available GeoServer workspaces</p>
                <p>Select the wonted workspace and Save again</p>
                <p>Logout and login again.</p>'
	);

  $form['geoserver_url'] = array(
      '#type' => 'textfield',
      '#title' => t('GeoServer URL'),
      '#default_value' => $url,
      '#description' => t('The URL where GeoServer is running. You need to re-login into Drupal to allow authentication to GeoServer before the new server can be used.'),
    );
   $form['geoserver_workspace'] = array(
      '#type' => $workspace_type,
      '#title' => t('GeoServer Workspace'),
      '#description' => t('The workspace where all layers and styles will be stored. Only changeable as long as the list of layers and styles is empty.'),
      '#options' => $workspace_type == 'select' ? $workspaces : NULL,
      '#default_value' => $workspace,
      '#disabled' => $workspace_type == 'textfield',
    );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  $form['reset'] = array(
    '#type' => 'submit',
    '#value' => t('Reset'),
  );

  return $form;
}

function geoserver_varaibles_form_submit($form, &$form_state){
  drupal_set_message("<pre>".print_r($form_state['values'],TRUE)."</pre>");
  if ($form_state['values']['op'] == 'Reset'){
    variable_del('geoserver_url');
    variable_del('geoserver_workspace');
    variable_del('geoserver_workspace_support');
  }
  if ($form_state['values']['op'] == 'Save'){
    $form_state['values']['geoserver_url'] = rtrim($form_state['values']['geoserver_url'], '/') . '/';
    variable_set('geoserver_url',$form_state['values']['geoserver_url']);
    if (!empty($form_state['values']['geoserver_workspace'])){
      variable_set('geoserver_workspace',$form_state['values']['geoserver_workspace']);
    }
  }
}
