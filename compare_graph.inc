<?php

function compare_graph(){
	plotly_js_load();
	/* jquery UI javascript for drag and drop */
	drupal_add_library('system', 'ui.draggable');
	drupal_add_library('system', 'ui.droppable');
	drupal_add_js(drupal_get_path('module', 'istsos') . '/js/istsosutils.js');
// 	// Add the javascript for multiselect
// 	drupal_add_js(drupal_get_path('module', 'grasp') . '/jslibraries/jquery-multiselect/jquery.multiselect.js');
//   drupal_add_css(drupal_get_path('module', 'grasp') . '/jslibraries/jquery-multiselect/jquery.multiselect.css');
//
// 	drupal_add_js(drupal_get_path('module', 'grasp') . '/jslibraries/jquery-multiselect/jquery.multiselect.filter.js'); // Libraries for add a filter field in the drop down header
//   drupal_add_css(drupal_get_path('module', 'grasp') . '/jslibraries/jquery-multiselect/jquery.multiselect.filter.css');
//
// 	drupal_add_css(drupal_get_path('module', 'grasp') .'/css/compare_graph.css');
//
// 	// Add the plugin for table scroll
// #	drupal_add_js(drupal_get_path('module', 'grasp') .'/jslibraries/underscore-1.5.2.min.js');
// 	drupal_add_js(drupal_get_path('module', 'grasp') .'/jslibraries/jquery.scrollTableBody-1.0.0.js');

	drupal_add_css(drupal_get_path('module', 'grasp') .'/css/grasp.css');
	// Add custom functionalities
	drupal_add_js(drupal_get_path('module', 'grasp') .'/js/compare_graph.js');

	$page = array();
	$page['graspcomparegraph-content'] = array(
    '#theme' => 'graspcomparegraph-content',        // this is the theme 'key' defined in 'mymodule_theme',
  );
	return $page;

 // 	$output = array();
  // $output['main_form'] = drupal_get_form("compare_graph_form");
  // $output['graph_form'] = drupal_get_form('istsos_sidebar_form');
  // return $output;

}

function proceduresgetfulllist(){

	  $url     = variable_get('istsos_url');
		$service = $_COOKIE['Drupal_visitor_istsosscheme'];
		$pl = istsos_REST_proceduregetlist($url, $service);
		// $offerings = array();
		// drupal_set_message("<pre>".print_r($pl,TRUE)."</pre>");
		if ($pl) {
			$json = drupal_json_encode($pl);
			drupal_json_output($json);
		}
}

function compare_graph_form($form,&$form_state){
	/**/
	/* This form is used to create a comparison graph platform */
  /**/

  $url     = variable_get('istsos_url');
	$service = $_COOKIE['Drupal_visitor_istsosscheme'];

	// get service description
	$service_desc = grasp_get_service_description($service);
#	drupal_set_title("Service $service - $service_desc");
	drupal_set_title("Compare charts");
	$form['description'] = array(
			'#markup' => "Scenario $service - $service_desc",
		);
	$form['help'] = array(
			'#prefix' => '<div class="description">',
			'#markup' => "Drag and drop variables or indicators into the two boxes to plot them over synchronous two-panel chart",
			'#suffix' => '</div>'
		);

	/* Return a list of procedures */
	$pl = istsos_REST_proceduregetlist($url, $service);
	$offerings = array();
	if ($pl) {
#			drupal_set_message("<pre>".print_r($pl['message'],TRUE)."</pre>");
		$temp = array('temporary');
		$form['wrapstart'] = array(
			'#markup' => '<div style="position:relative;">',
		);
		foreach ($pl['data'] as $procedure){
				$pon = array_values(array_diff_assoc($procedure['offerings'],$temp));// get the procedure offering excluding the temporary one
#				drupal_set_message("<pre>".print_r($pon,TRUE)."</pre>");
				$proc_offerings[] = $pon[0];
				$offerings_dom = array_unique($proc_offerings); // The offerings domain

				$proc_list[$procedure['name']] = array(
					'proc_name' => array(
						'data' => array(
							'#markup'	=> '<div id="'.$procedure['name'].'" class="draggable">'.$procedure['name'].'</div>',
						),
					),
					'proc_descr' => array(
						'data' => array(
							'#markup'	=> $procedure['description'],
						),
					),

				);

		}

		$header = array(
			'proc_name' => "Procedure",
			'proc_descr' => "Description",
		);

		$form['procedures'] = array(
			'#prefix' => '<div style="width:50%; float:left;">',
			'#suffix' => '</div>',
			'#theme' => 'table',
			'#header' => $header,
			'#rows' => $proc_list,
#			'#default_value' => $a,
			'#empty' => t('No procedures found.'),
			'#js_select' => FALSE,
			'#attributes' => array('class' => array('small-table')),
		);

		$form['dropg1'] = array(
			'#prefix' => '<div id="dropwrap" style="width:30%; float:left; margin-left:1.4em; margin-top:0.6em; margin-bottom:0.6em; margin-right:0.6em;" >',
			'#markup' => '<div id="dg1" class="droppable">Graph 1 :</div>'
		);

		$form['dropg2'] = array(
#			'#suffix' => '</div>',
			'#markup' => '<div id="dg2" class="droppable">Graph 2 :</div>'
		);

		$form['Alist'] = array(
			'#suffix' => '</div>',
			'#markup' => '<div id="obspro_list"></div>'
		);

		$form['reload'] = array(
			'#type' => 'button',
			'#value' => 'Refresh',
			'#submit' => array('cg_reload'),
#			'#attributes' =>  array('class' => array('reloadbtn')),
		);

		$form['home'] = array(
			'#type' => 'submit',
			'#value' => 'Home',
			'#submit' => array('cg_home'),
#			'#attributes' =>  array('class' => array('reloadbtn')),
		);


		$form['wrapstop'] = array(
			'#markup' => '</div>',
		);

	} else {
		drupal_set_message("Can't get information about istSOS server", 'error');
	}

  return $form;
}

function cg_reload(){
	drupal_goto('?q=graph_compare');
}

function cg_home(){
#	drupal_set_message('called');
	drupal_goto('<front>');
}

function istsos_sidebar_form($form,&$form_state){

	$form['chart_container'] = array(
#		'#prefix' => '<div id="side_panel" style="margin-top: 1.4em;">',
#		'#suffix' => '</div>',
		'#markup' => '
				<div id="chart_container">
					<div id="y_axis"></div>
					<div id="chartdg1"></div><div id="legendOne"></div><br />
					<div id="y_axis2"></div>
					<div id="chartdg2"></div><div id="legendTwo"></div><br />
					<div id="slider"></div><br />
					<!-- <div id="legend"></div> -->
				</div>
	');
	return $form;
}

function compare_graph_form_submit ($form,&$form_state){

#  drupal_set_message("<pre>".print_r($form_state['values'],TRUE)."</pre>");

  foreach($form_state['values']['procedures'] as $proc_name){
  	if ( $proc_name != 0 ){

  	}
  }
	$form_state['rebuild'] = TRUE;
}

/* Callback from compare_graph.js */
function compare_graph_callback() {
	$url     = variable_get('istsos_url');
	$service = $_COOKIE['Drupal_visitor_istsosscheme'];
	$prc_name = $_POST['procname'];
	$serie = $_POST['serie'];
	$prc_mbl = array();

	/* from the procedure name verify if is inside the daily offering */
	$prc_mbl = istsos_REST_prcmemberlist($service, 'daily');
	foreach ($prc_mbl['data'] as $values){
		$prc_array[] = $values['name'];
	}
#	drupal_set_message("prc array <pre>".print_r($prc_array,TRUE)."</pre>");
	if (in_array($prc_name, $prc_array)){
		$graph_renderer = 'line';
	} else {
		$graph_renderer = 'bar';
	}

	/* get eventtime and observed properties */
	$procedure = istsos_REST_proceduredetails($service, $prc_name);
	foreach($procedure['data']['outputs'] as $key => $value){
		if ($key == 0) { // time with constraint
			if (isset($value['constraint'])){
				$b = $value['constraint']['interval'][0]; // go back one day from the begin value otherwise the begin value is not passed
				$begin = date('c',(strtotime ( '-1 day' , strtotime ( $b) ) ));
				$end   = $value['constraint']['interval'][1];
				$eventtime = array($begin,$end);
			}
		}
		$ob_prop_array[] = $value['definition'];
	}

	if (empty($_POST['opname'])){
#		$op_name = "A1:A1" ;
		$op_urn = $ob_prop_array[1];
	} else {
		$op_name = $_POST['opname'];
		$op_urn = 'urn:ogc:def:parameter:x-istsos:1.0:'.$op_name;
	}

	$obs = istsos_REST_getobservation ($service, 'temporary', $prc_name, $op_urn, $eventtime);
	if (!empty($obs['data'][0]['result']['DataArray']['values'])){
			$miny = 0;
			foreach ($obs['data'][0]['result']['DataArray']['values'] as $values){
				/* Get min and max during the loop cicle */
				if ($miny == 0 ){
					$miny = floatval($values[1]);
					$maxy = $miny;
				} elseif ($miny > floatval($values[1])) {
					$miny = floatval($values[1]);
				}
				if ($maxy < floatval($values[1])){
					$maxy = floatval($values[1]);
				}
				/**/
				$date = strtotime($values[0]);
				$rows[] = array('x' => $date, 'y' => floatval($values[1]));
			}
			foreach ($obs['data'][0]['result']['DataArray']['field'] as $a => $fields){
				if (($a % 2) == 1){
					$single = explode(':', $fields['name']);
					$data = array('name' => $single[1], 'data'=> $rows);
				}
			}
			drupal_json_output(array($ob_prop_array, $data, $miny, $maxy, $serie, $graph_renderer));
		}	else {
			// return some data to javascript side with the error
			drupal_json_output(array('error'=>'Verify on reports if you have rights to acess the timeperiod you requested.'));
		}
}

function compare_graph_th(){
  // Call theme() function, so that Drupal includes the custom-page.tpl.php template
#  return theme('my_custom_template');

  $url     = variable_get('istsos_url');
	$service = $_COOKIE['Drupal_visitor_istsosscheme'];

	/* Return a list of procedures */
	$pl = istsos_REST_proceduregetlist($service);
	if ($pl){
#		drupal_set_message($pl['message']);
		$temp = array('temporary');
				$header = array(
					'proc_name' => "Procedure",
					'proc_descr' => "Description",
		);
		foreach ($pl['data'] as $procedure){
				//$procedure['samplingTime']; // array beginposition - endposition
				//$procedure['observedproperties']; // array
				//$procedure['description'];
				//$procedure['name'];
				//

				$pon = array_values(array_diff_assoc($procedure['offerings'],$temp));// get the procedure offering excluding the temporary one
				$proc_offerings[] = $pon[0];

#				drupal_set_message("proc <pre>".print_r($proc_offerings,TRUE)."</pre>");

				$proc_list[$procedure['name']] = array(
					'proc_name' => array(
						'data' => array(
							'#markup'	=> $procedure['name'],
						),
					),
					'proc_descr' => array(
						'data' => array(
							'#markup'	=> $procedure['description'],
						),
					),

				);

		}
		$offerings_dom = array_unique($proc_offerings);
		return theme('table', array('header' => $header, 'rows' => $proc_list));
	}

}
