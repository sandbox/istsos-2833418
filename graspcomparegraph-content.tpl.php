<?php
/**
 * @file template file for 'graspcomparegraph-content' theme
 *
 */
?>
<!-- Modified template -->
<?php if (user_is_logged_in()) : ?>
  <!-- Description of the service and of the functionalities -->
  <?php
    $url     = variable_get('istsos_url');
  	$service = $_COOKIE['Drupal_visitor_istsosscheme'];
  	// get service description
  	$service_desc = grasp_get_service_description($service);
    print '<h3>Servizio : '.$service_desc.'</h3>';
  ?>

  <div class="row">
    <div class="col-md-3">
      <!-- D&D elements -->
      <div id="dropwrap" style="padding-top: 13%;" >
        <div class="row" style="padding: 0 0 10px;"><div class="col-md-4 col-sm-4" style="padding: 10px;">Graph 1 :</div><div id="dg1" class="col-md-8 col-sm-8 droppable fill">Drop here</div></div>
        <div class="row"><div class="col-md-4 col-sm-4" style="padding: 10px;">Graph 2 :</div><div id="dg2" class="col-md-8 col-sm-8 droppable fill">Drop here</div></div>
      </div>
    </div>
    <div class="col-md-9">
      <!-- Scrollable table -->
      <table id='tbl_cnt' class="table table-striped">
        <thead><tr>
          <th>Procedure name</th>
          <th>Procedure description</th>
          <th>Offering</th>
        </tr></thead><tbody></tbody>
      </table>
    </div>
  </div>
  <!-- List of observed prorperties -->
  <div id="obspro_list"></div>

  <div class="row">
    <div id="chartdg1"></div>
    <div id="chartdg2"></div>
  </div>
<?php endif; ?>

<!-- END  Modified template -->
