<?php


/* Menu callback called from ol_grasp_queryfromslidebar.js when moving from the Project Hierarchy modal to the Sector modal */
/* returned values */
/* @ description - the sector description */
/* @ markers_markup - a markup list of markers attached to this sector */
/* @ markers_ids - list of markers nid */
function sector_ph(){
	$nid = $_POST['nid']; // Sector node id
	/* Get content with the description of the sector given the sector node id */
	$node = node_load($nid, NULL, FALSE);
	$description = $node->field_sector_desc[$node->language][0]['value'];
	/* Get the list of marker related to this sector and create markup foreach marker */
	foreach ($node->field_marker_list[$node->language] as $marker){
		$marker_node = node_load($marker['target_id'], NULL, FALSE);
		// drupal_set_message("<pre>".print_r($marker_node,TRUE)."</pre>");
		$FOI_markup[] = '<li><a class="foilink" href="#" id="'.$marker_node->field_short_label[$marker_node->language][0]['value'].
                    '" nid="'.$marker['target_id'].'" name="'.$marker_node->title.'">'.$marker_node->title.' - '.$marker_node->field_description_short[$marker_node->language][0]['value'].
                    '</a></li>';
		$FOI_names[] = $marker_node->title;
	}

	$response = array('description' => $description, 'markers_markup' => $FOI_markup, 'markers_names' => $FOI_names );
  $json = drupal_json_encode($response);
	drupal_json_output($json);
}

/* Menu callback called from ol_grasp_queryfromslidebar.js when cliccking on marker link */
/* Get all the procedures related to all the markers/FOI attached to the sector */
/* Passing a list of FOI names make a cicle to the getFeatureOfInterest request */
function sector_ph_procedures(){
	$url     = variable_get('istsos_url');
	$service = $_COOKIE['Drupal_visitor_istsosscheme'];
  $field   = variable_get('istsos_pg_field');
	// get the srid of the configured postgis field
  $field_info = field_info_field($field);
  $srid = $field_info['settings']['srid'];

	$SectorNid = $_POST['sectornid'];
	$selectedFOIName = $_POST['foiname'];
	$selectedFOINid = $_POST['foinid'];
	$FOI_list = $_POST['foilist'];

	// Get the description of this FOI given the $selectedFOINid
	$mk_node = node_load($selectedFOINid, NULL, FALSE);
	$mk_description = $mk_node->field_description_text[$mk_node->language][0]['value'];

	// Get a list of offerings/frequencies
	$fulloffelist = istsos_REST_offeringlist($url, $service);
	// drupal_set_message("fulloffelist <pre>".print_r($fulloffelist,TRUE)."</pre>");
	foreach ($fulloffelist['data'] as $key => $value){
		if ($value['name'] !== "temporary"){
			$thisprc = istsos_REST_prcmemberlist($url, $service, $value['name']);
			// drupal_set_message("this <pre>".print_r($thisprc,TRUE)."</pre>");
			foreach($thisprc['data'] as $prk => $prval){
				// This is valid because for grasp a procedure can be only in one frequency/offering
				$list[$prval['name']] = $prval['offerings'][1];
			}
		}
	}
	// drupal_set_message("prcl <pre>".print_r($prc,TRUE)."</pre>");
	// drupal_set_message("oprcl <pre>".print_r($oprc,TRUE)."</pre>");

	// Get a differentiate list of procedures attached to the sector
	foreach ($FOI_list as $FOI_name) {
		// use the Enhanced functionality GetFeatureOfInterest to get a list of procedures attached to the FOI
		// We have to output the procedure description
		if ($FOI_name != $selectedFOIName) {
			$procedures_list = istsos_GetFeatureOfInterest($url, $service, $FOI_name, $srid);

			if (isset($procedures_list[$service])){
	      foreach ($procedures_list[$service] as $value){
	        // Get details about each procedure to get the procedure description
	        $procedure = istsos_REST_proceduredetails($url, $service, $value);
					// drupal_set_message("other procedure <pre>".print_r($procedure,TRUE)."</pre>");
	        if ($procedure) {
						$other_foi_procedures[$procedure['data']['system']] = array("description" => $procedure['data']['description'], "frequency" => $list[$procedure['data']['system']]);
					}
				}
			}
		} else {
			$procedures_list = istsos_GetFeatureOfInterest($url, $service, $FOI_name, $srid);
			if (isset($procedures_list[$service])){
	      foreach ($procedures_list[$service] as $value){
	        // Get details about each procedure to get the procedure description
	        $procedure = istsos_REST_proceduredetails($url, $service, $value);
					// drupal_set_message("this procedure <pre>".print_r($procedure,TRUE)."</pre>");
	        if ($procedure) {
						//$this_foi_procedures[$procedure['data']['system']] = $procedure['data']['description'];
						$this_foi_procedures[$procedure['data']['system']] = array("description" => $procedure['data']['description'], "frequency" => $list[$procedure['data']['system']]);
					}
				}
			}
		}
	}

	// drupal_set_message("thisfoi prc <pre>".print_r($other_foi_procedures,TRUE)."</pre>");
	// drupal_set_message("otherfoi prc <pre>".print_r($this_foi_procedures,TRUE)."</pre>");


	$json = drupal_json_encode(array("description" => $mk_description, "indicators" => $this_foi_procedures, "otherindicators" => $other_foi_procedures));
	drupal_json_output($json);
}

// Return the sector nid given one of the attached FOI nid
function sector_marker() {
	$marker_nid = $_POST['foinid'];
	$marker_name = node_load($marker_nid)->title;

	$query = new EntityFieldQuery();
	$query->entityCondition('entity_type', 'node')
		->entityCondition('bundle', 'sector')
		->propertyCondition('status', 1)
		->fieldCondition('field_marker_list', 'target_id', $marker_nid, '=');
	$result = $query->execute();

	if (isset($result['node'])) {
			$sector_nid = array_keys($result['node']);
			$node = node_load($sector_nid[0]);
			foreach ($node->field_marker_list[$node->language] as $marker){
				$marker_node = node_load($marker['target_id'], NULL, FALSE);
				$FOI_names[] = $marker_node->title;
			}
			$json = drupal_json_encode(array("response" => "success" ,"sector_nid" => $sector_nid[0], "sector_name" => $node->title, "selected_marker" => $marker_name, 'markers_names' => $FOI_names));
			drupal_json_output($json);
	} else {
		$json = drupal_json_encode(array("response" => "error", "message" => "This marker is of type Sector but is not attached to any Sector."));
		drupal_json_output($json);
	}
}

// Return a list of observed properties for Indicator modal window
// The list is inserted inside the multiselect widget
function sector_ph_op_list() {
	$url     = variable_get('istsos_url');
	$service = $_COOKIE['Drupal_visitor_istsosscheme'];
	$prc_name = $_POST['procname'];

	$procedure = istsos_REST_proceduredetails($url, $service, $prc_name);
	// drupal_set_message('Debug procedures op list <pre>'.print_r($procedure,TRUE).'</pre>');

	if ($procedure['message']) { // TODO aggiungere == succesfully
		if(isset($procedure['data']['outputs'][0]['constraint'])){
			$begin = $procedure['data']['outputs'][0]['constraint']['interval'][0];
			$end   = $procedure['data']['outputs'][0]['constraint']['interval'][1];
			// Set observed properties name list
			$op = array();
			foreach($procedure['data']['outputs'] as $key => $val){
				if ($key != 0){
					$op[$procedure['data']['outputs'][$key]['definition']] = $procedure['data']['outputs'][$key]['name'];
				}
			}
			$json = drupal_json_encode(array("response" => "success", "op_list" => $op, "eventtime" => array($begin,$end)));
			drupal_json_output($json);
		} else {
			$json = drupal_json_encode(array("response" => "error", "message" => "Time constraint not setted - Error on import the procedure."));
			drupal_json_output($json);
		}
	} else {
		$json = drupal_json_encode(array("response" => "error", "message" => "ERROR loading observed properties list."));
		drupal_json_output($json);
	}
}

// Return a getobservation response to be plotted
function sector_ph_getobservation() {
	$url     = variable_get('istsos_url');
	$service = $_COOKIE['Drupal_visitor_istsosscheme'];
	$offe_name = $_POST['offename'];
	// $offe_name = "temporary";
	$prc_name = $_POST['procname'];
	$ob_prop = $_POST['opdef'];
	$start = $_POST['start'];
	$end = $_POST['end'];
	$graphtype = $_POST['graphtype'];

	$eventtime = array($start, $end);
	// drupal_set_message("istsos_graph.inc ofeering name <pre>".print_r($offe_name,TRUE)."</pre>");
	//
	// Call to getobservation
	//
	$observation = istsos_REST_getobservation ($url, $service, $offe_name, $prc_name, $ob_prop, $eventtime);
	if ($observation) {
			// drupal_set_message($observation['message']);
			$json = drupal_json_encode(array("response" => "success", "graphtype" => variable_get($graphtype) ,"observations" => $observation['data'][0]));
			drupal_json_output($json);
	} else {
		drupal_set_message('istsos_REST_getobservation FAILED','error');
		$json = drupal_json_encode(array("response" => "error", "message" => "Cannot load observation for procedure ".$prc_name.""));
		drupal_json_output($json);
	}
}

// Return the short description and the long description of the indicator slected
function sector_ind_desc(){
	$ind_title = $_POST['title'];
	$query = new EntityFieldQuery();
	$query->entityCondition('entity_type', 'node')
		->entityCondition('bundle', 'indicator')
		->propertyCondition('title', $ind_title)
		->propertyCondition('status', 1);
	$result = $query->execute();

	if (isset($result['node'])) {
		$val = 	array_values($result['node']);
		$node = node_load($val[0]->nid);
		$long_d = $node->field_ind_desc[$node->language][0]['value'];
		$short_d = $node->field_ind_desc_short[$node->language][0]['value'];
		$json = drupal_json_encode(array("response" => "success", "shortDesc" => $short_d, "longDesc" => $long_d));
		drupal_json_output($json);
	} else {
		$json = drupal_json_encode(array("response" => "error", "message" => "Cannot find an Indicator content with Indicator name ".$ind_title.""));
		drupal_json_output($json);
	}
}
