<?php

/* Menu callback called from ol_grasp_popupmw.js when cliccking on marker link of type component */
/* Get all the procedures related to this markers/FOI */
function component_procedures(){
  $url     = variable_get('istsos_url');
  $service = $_COOKIE['Drupal_visitor_istsosscheme'];
  $field   = variable_get('istsos_pg_field');
  // get the srid of the configured postgis field
  $field_info = field_info_field($field);
  $srid = $field_info['settings']['srid'];

  $selectedFOIName = $_POST['foiname'];
  $selectedFOINid = $_POST['foinid'];

  // Get the long description : field_description_text and short description : field_description_short of this FOI given the $selectedFOINid
  $mk_node = node_load($selectedFOINid, NULL, FALSE);
  $mk_sh_description = $mk_node->field_description_short[$mk_node->language][0]['value'];
  $mk_description = $mk_node->field_description_text[$mk_node->language][0]['value'];

  // Get a list of offerings/frequencies
  $fulloffelist = istsos_REST_offeringlist($url, $service);
  // drupal_set_message("fulloffelist <pre>".print_r($fulloffelist,TRUE)."</pre>");
  foreach ($fulloffelist['data'] as $key => $value){
    if ($value['name'] !== "temporary"){
      $thisprc = istsos_REST_prcmemberlist($url, $service, $value['name']);
      // drupal_set_message("offerings <pre>".print_r($thisprc,TRUE)."</pre>");
      foreach($thisprc['data'] as $prk => $prval){
        // This is valid because for grasp a procedure can be only in one frequency/offering
        $list[$prval['name']] = $prval['offerings'][1];
      }
    }
  }

  // Get a list of procedures related to this marker
  $procedures_list = istsos_GetFeatureOfInterest($url, $service, $selectedFOIName, $srid);
  if (isset($procedures_list[$service])){
    foreach ($procedures_list[$service] as $value){
      // Get details about each procedure to get the procedure description
      $procedure = istsos_REST_proceduredetails($url, $service, $value);
      // drupal_set_message("Componet procedures <pre>".print_r($procedure,TRUE)."</pre>");
      if ($procedure) {
        $this_foi_procedures[$procedure['data']['system']] = array("description" => $procedure['data']['description'], "frequency" => $list[$procedure['data']['system']]);
      }
    }
  }

  // The marker/FOI long and short description are used to create an accordion
  //
  $json = drupal_json_encode(array("description" => $mk_description, "shdescription" => $mk_sh_description, "procedures" => $this_foi_procedures));
	drupal_json_output($json);
}
