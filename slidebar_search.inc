<?php

// Make a search on nodes and return a list of node_title to be shown in the slidebar
function slidebar_search_page(){

	$keys = $_POST['keys'];
	$projection = $_POST['projection'];
	$results = node_search_execute($keys);
	$appo = explode(':', $projection);
	$epsg = $appo[1];
	$istsos_pg_field = variable_get('istsos_pg_field','');

	$back_data = array();
	foreach ($results as $result){
		if ( property_exists($result['node'], $istsos_pg_field) ){
			$title = $result['node']->title;
			$result = db_query("SELECT ST_AsText(ST_Transform(geometry('".$result['node']->$istsos_pg_field['und'][0]['geometry']."'),$epsg));");
	  	$db_entry = $result->fetchAll();
			$appo = explode('(', $db_entry[0]->st_astext);
			$appo2 = explode(' ', $appo[1]);
			$lon = $appo2[0];
			$lat = trim($appo2[1],')');
			$back_data[] = array('title' => $title, 'lat' => $lat, 'lon' => $lon);
		}
	}
	drupal_json_output($back_data);
}
