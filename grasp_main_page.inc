<?php


function grasp_main_page(){
	plotly_js_load();
	drupal_add_css(drupal_get_path('module', 'grasp') . '/css/grasp.css');

	// drupal_add_js(drupal_get_path('module', 'plotlymapping') . '/js/map_plotly.js');
  // drupal_add_library('system', 'effects.slide'); // slider effect
  // drupal_add_js(drupal_get_path('module', 'grasp') . '/js/slidebar.js');
	// $page is an array of 'page elements' (see the Form API for a complete list of pre-defined elements)
  $page = array();

  // gather the data you need to be sent to your template file
  // starting with $myvar1 (defined in the hook_theme implementation above ^)
  // $myvar1 = t('Hello World');        // t() is Drupal's "Translation" function
  // $myvar2 = array(
  //   'option1' => t('Option 1'),
  //   'option2' => t('This is how we say Hello World in Drupal land!'),
  // );
  // $myobj = new stdClass();
  // $myobj->thisData = 'This Data';
  // $myobj->thatData = 'That Data';


	// drupal_set_message('<pre>'.print_r(gettype($mapexist),TRUE).'</pre>');
  // Next, we're going to add a page element that Drupal will render..
  $page['graspmainpage-content'] = array(
    '#theme' => 'graspmainpage-content',        // this is the theme 'key' defined in 'mymodule_theme',
		// '#mapexist' => $mapexist,
		// '#myvar1' => $myvar1,        // remember the 'myvar1' variable key...?
    // '#myvar2' => $myvar2,
    // '#myobj' => $myobj,
  );
  # NOTE: Drupal will correctly parse all array keys that begin with '#' and process them accordingly.

  # NOTE 2: The above can also be done like this -- it's a matter of personal taste:
  // $page['bio-content'] = theme('bio-content', array('myvar1' => $myvar1, 'myvar2' => $myvar2, 'myobj' => $myobj));

  return $page;
}
