/* This behavior contains functionalitis to mange the modal windows related to sectors */
/* The Sector modal windows functionalities are shared between the two GRASP openlayers behaviors */


// TODO this behavior is used for both sector/indicators and components rename it in Drupal.behaviors.graspMW
(function ($) {
  Drupal.behaviors.graspSectorMW = {
    attach: function (context, settings) {
      Drupal.behaviors.graspSectorMW.ServiceName = $.cookie("Drupal.visitor.istsosdescr") || {};
      Drupal.behaviors.graspSectorMW.SectorName = Drupal.behaviors.graspSectorMW.SectorName || {};
      Drupal.behaviors.graspSectorMW.SectorNodeUrl = Drupal.behaviors.graspSectorMW.SectorNodeUrl || {};
      Drupal.behaviors.graspSectorMW.SectorNid = Drupal.behaviors.graspSectorMW.SectorNid || {};
      Drupal.behaviors.graspSectorMW.FOINid = Drupal.behaviors.graspSectorMW.FOINid || {};
      Drupal.behaviors.graspSectorMW.FOIName = Drupal.behaviors.graspSectorMW.FOIName || {};
      Drupal.behaviors.graspSectorMW.FOIList = Drupal.behaviors.graspSectorMW.FOIList || {};
      Drupal.behaviors.graspSectorMW.PRCName = Drupal.behaviors.graspSectorMW.PRCName || {};
      Drupal.behaviors.graspSectorMW.PRCOffering = Drupal.behaviors.graspSectorMW.PRCOffering || {};
      Drupal.behaviors.graspSectorMW.begin = Drupal.behaviors.graspSectorMW.begin || {};
      Drupal.behaviors.graspSectorMW.end = Drupal.behaviors.graspSectorMW.end || {};
      Drupal.behaviors.graspSectorMW.traces = Drupal.behaviors.graspSectorMW.traces || [];
      Drupal.behaviors.graspSectorMW.layout = Drupal.behaviors.graspSectorMW.layout || {};
      Drupal.behaviors.graspSectorMW.IndShortDesc = Drupal.behaviors.graspSectorMW.IndShortDesc || {};
      Drupal.behaviors.graspSectorMW.IndLongDesc = Drupal.behaviors.graspSectorMW.IndLongDesc || {};
      Drupal.behaviors.graspSectorMW.Indicators = Drupal.behaviors.graspSectorMW.Indicators || {};
      Drupal.behaviors.graspSectorMW.OtherIndicators = Drupal.behaviors.graspSectorMW.OtherIndicators || {};

      /* Prepare the SectorMW */
      Drupal.behaviors.graspSectorMW.SectorMWPrepare = function(){
        // close the Project tree graph popup - STEP 1 CLOSE
  			$('#ProjectTree_MW').modal('hide');
  			// open the sector dialog window - STEP 2 OPEN
  			$('#Sector_MW').modal('show');
  			// set the Sector modal window title
  			$('#SLabel').text('Sector : '+ Drupal.behaviors.graspSectorMW.SectorName );
  			// Empty the #Sdescription, #SHTree and #SFOI before redraw
  			$('#Sdescription').empty();
  			$('#SHTree').empty();
  			$('#SFOI').empty();
      };
      /* Prepare the SSectorMW */
      Drupal.behaviors.graspSectorMW.SSectorMWPrepare = function(){
        Drupal.openlayers.popup.onUnselect();
        // close the sector dialog window - STEP 2 CLOSE
        $('#Sector_MW').modal( "hide" );
        // open the sub sector dialog window - STEP 3 OPEN
        $('#SSector_MW').modal('show');
        // Empty #SSHTree, #SSIndMark and #SSIndOther before to redraw
        $('#SSHTree').empty();
        $('#SSIndMark').empty();
        $('#SSIndOther').empty();
        // set the SubSector modal window title to the title of the seleced marker/FOI
        $('#SSLabel').text('Sector : '+ Drupal.behaviors.graspSectorMW.FOIName );
        // set the back button
        $('#BTStep2').html('Back to '+ Drupal.behaviors.graspSectorMW.SectorName +' sector');
      };
      /* Prepare the IndicatoMW */
      Drupal.behaviors.graspSectorMW.IndicatorMWPrepare = function(){

        console.log('--------------------------------------------------');
        console.log(Drupal.behaviors.graspSectorMW.Indicators);
        console.log(Drupal.behaviors.graspSectorMW.OtherIndicators);
        // TODO Fill the frequencies radio buttons Horizons and Other frequencies

        // get the Indicator short and long descriptyions given the selected procedure name that is the drupalnode->indicator->title
        // TODO We are queering for the drupalnode->indicator description - is better to update the procedure description
        $.ajax({
            url: "?q=grasp_sector_ind_desc", // function sector_ind_desc in sector.inc
            type: "POST",
            data: {title: Drupal.behaviors.graspSectorMW.PRCName},
            dataType:'json',
            async: false,
            success: indicatorDescriptions,
        });

        function indicatorDescriptions (data, textStatus, jqXHR) {
          var pjd = $.parseJSON(data);
          console.log(pjd);
          if (pjd.response == "success"){
            // TODO We are queering for the indicator node description - is better to update the procedure description
            Drupal.behaviors.graspSectorMW.IndShortDesc = pjd.shortDesc;
            Drupal.behaviors.graspSectorMW.IndLongDesc = pjd.longDesc;
            // close the sub sector modal window - STEP 2 CLOSE
            $('#SSector_MW').modal( "hide" );
            // open the indicator modal window - STEP 3 OPEN
            $('#Indicator_MW').modal('show');
            // Empty observed properties list and traces
            $('#Indicator_op_list').empty();
            Drupal.behaviors.graspSectorMW.traces = [];
            // set the Indicator modal window title
            $('#IndLabel').text('Indicator: '+ Drupal.behaviors.graspSectorMW.IndShortDesc +' - Scenario: '+ Drupal.behaviors.graspSectorMW.ServiceName);
            // set the accordion description
            $('#indDesc').html(Drupal.behaviors.graspSectorMW.IndShortDesc);
            $('#indLongDesc').html(Drupal.behaviors.graspSectorMW.IndLongDesc);
            // set the back button
            $('#BTStep3').html('Back to '+ Drupal.behaviors.graspSectorMW.FOIName +' sector');
          } else {
            alert(pjd.message);
          }
        }
      };
      /* Prepare the ComponentMW */
      Drupal.behaviors.graspSectorMW.ComponentMWPrepare = function(){
        // Call the unselectAll to fix Issue #2842945
        Drupal.openlayers.popup.onUnselect();
        // Empty elements before to redraw
        $('#compDesc').empty();
        $('#compLongDesc').empty();
        $('#comp_prc_list').empty();
        $('#Component_op_list').empty();
        $('#Component_op_graph').empty();
        // Reset the traces array
        Drupal.behaviors.graspSectorMW.traces = [];
        // set the Compponent modal window title to the title of the seleced marker/FOI
        $('#ComponentLabel').text('Component : '+ Drupal.behaviors.graspSectorMW.FOIName +' - Scenario : '+ Drupal.behaviors.graspSectorMW.ServiceName);
      };

      /* Called when switching radio buttons procedures/frequencies */
      Drupal.behaviors.graspSectorMW.ComponentMWFrequenciesClean = function(){
        $('#Component_op_list').empty();
        $('#Component_op_graph').empty();
        // Reset the traces array
        Drupal.behaviors.graspSectorMW.traces = [];
      };


      // Fill the Sector modal window with:
      // Sector description
      // A list of FOI attached to this Sector
      Drupal.behaviors.graspSectorMW.SectorMWFill = function(){
        $.ajax({
            url: "?q=grasp_sector_ph", // function sector_ph in sector.inc
            type: "POST",
            data: {nid: Drupal.behaviors.graspSectorMW.SectorNid},
            dataType:'json',
            async: false,
            success: sectorMwContent,
        });

        function sectorMwContent (data, textStatus, jqXHR) {
          console.log('Sector callback data');

          var pjd = $.parseJSON(data)
          console.log(pjd);
          $('#Sdescription').append( pjd.description );// pjd.description contains the sector description

          /* 1 - Set an ul of Feature of Interest links and marker description */
          $("#SFOI").append('<ul>');
          $.each(pjd.markers_markup, function (namePrc, value) {
                // console.log(value);
                $("#SFOI").append(value);
          });
          $("#SFOI").append('</ul>');

          Drupal.behaviors.graspSectorMW.FOIList = pjd.markers_names;
          /*  Attach click functionality to the Features of interest links */
          $('.foilink').click(function () {
            // Get values from the Features of interest link attributes
            console.log('link clicked');
            //FOIName = $(this).text();// MarkerName or FOIName
            //console.log(FOIName);
            //var FOIDescr = $(this).next().text();
            Drupal.behaviors.graspSectorMW.FOIName = $(this).attr('name');
            // console.log(FOIName);
            Drupal.behaviors.graspSectorMW.FOINid = $(this).attr('nid');
            //FOIShortLabel = $(this).attr('id');

            // Prepare the SSector Modal Window
            Drupal.behaviors.graspSectorMW.SSectorMWPrepare();
            // Call the tree graph function for the clicked sub sector
            Drupal.behaviors.graspD3TreeGraph.D3TreeProjectGraph(Drupal.behaviors.graspSectorMW.SectorNid, '#SSHTree');
            // Fill the sub sector modal window
            Drupal.behaviors.graspSectorMW.SSectorMWFill();

          });
        }
      };

      // Fill the Sub Sector modal window with:
      // the description of the FOI selected
      // a list of indicators related to the selected FOI
      // a list of indicators NOT related to the selected FOI
      Drupal.behaviors.graspSectorMW.SSectorMWFill = function(){
        // get a list of procedures attached to this Sector
        // console.log(Drupal.behaviors.graspSectorMW.FOINid);
        $.ajax({
          url: "?q=grasp_sector_ph_procedures",
          type: "POST",
          data: {sectornid:Drupal.behaviors.graspSectorMW.SectorNid, foinid : Drupal.behaviors.graspSectorMW.FOINid, foiname: Drupal.behaviors.graspSectorMW.FOIName, foilist: Drupal.behaviors.graspSectorMW.FOIList},
          dataType:'json',
          async: false,
          success: subSectorMWContent,
        });

        function subSectorMWContent (data, textStatus, jqXHR) {
          var pjd = $.parseJSON(data)
          console.log(pjd);
          // TODO add the description of the selected marker field_description_text attached to this sector
          $("#SSdescription").append(pjd.description);
          // Fill the accordion with indicators and otherindicators
          if (pjd.indicators !== null){
            // TODO indicator == procedure
            Drupal.behaviors.graspSectorMW.Indicators = pjd.indicators;
            $.each(pjd.indicators, function (namePrc, obj) {
              $("#SSIndMark").append('<li><a class="indicator" frequency="'+obj.frequency+'" name="'+namePrc+'" href="#">'+obj.description+'</a></li>');
            });
          } else {
            $("#SSIndMark").append('<li>No indicators for this marker</li>');
          }
          if (pjd.otherindicators !== null){
            // TODO otherindicators == otherprocedures
            Drupal.behaviors.graspSectorMW.OtherIndicators = pjd.otherindicators;
            $.each(pjd.otherindicators, function (namePrc, obj) {
              $("#SSIndOther").append('<li><a class="indicator" frequency="'+obj.frequency+'" name="'+namePrc+'" href="#">'+obj.description+'</a></li>');;
            });
          } else {
            $("#SSIndOther").append('<li>No other indicators</li>');
          }
          // Add click functionalities to the indicator class links
          $('.indicator').click(function () {
            console.log('link clicked');
            // Get name and frequency of indicator/procedure
            Drupal.behaviors.graspSectorMW.PRCName = $(this).attr('name');
            Drupal.behaviors.graspSectorMW.PRCOffering = $(this).attr('frequency');
            // Get the short descritpion of the indicator node with the same procedure name
            Drupal.behaviors.graspSectorMW.IndicatorMWPrepare();
            console.log("IndicatorMWPrepare - exit");
            // Fill the indicator modal window with data related to the indicator/procedure selected
            Drupal.behaviors.graspSectorMW.PlotlyMWFill('Indicator');
          });

        };
      };

      // Fill the modal window with :
      // the graph of the first alternative
      // the alternatives selector
      Drupal.behaviors.graspSectorMW.PlotlyMWFill = function(element){
        // get a list of alternatives attached to this procedure
        console.log('PlotlyMWFill');
        $.ajax({
          url: "?q=grasp_sector_ph_op_list",
          data: {procname:Drupal.behaviors.graspSectorMW.PRCName, },
          type: "POST",
          dataType:'json',
          async: false,
          success: FillMWGraph,
        });

        function FillMWGraph (data, textStatus, jqXHR) {
          console.log('PlotlyMWFill - FillMWGraph');
          var pjd = $.parseJSON(data)
          console.log(pjd);
            if (pjd.response == "success"){
              // TODO add graph_default_style
              // Drupal.behaviors.graspPlotly.DefaultStyle = bar;

              // set begin and end eventtime
              Drupal.behaviors.graspSectorMW.begin = pjd.eventtime[0];
              Drupal.behaviors.graspSectorMW.end = pjd.eventtime[1];
              //
              // Build the multiselect widget
              //
                var s = $('<select/>');
                $.each(pjd.op_list, function (opdef, opname){
                  // opname is name:name get just name
                  s.append($('<option value="'+opdef+'"/>').html(opname.split(":")[1]));
                });
      					$('#'+element+'_op_list').append(s);

                $('#'+element+'_op_list > select').multiselect({
                  appendTo: '#'+element+'_MW',
                  header: false,
                }).multiselectfilter();
              // attach the event handler when a checkbox is checked
                $('#'+element+'_op_list > select').on("multiselectclick", function(event, ui) {
                  /* event: the original event object
                  ui.value: value of the checkbox
                  ui.text: text of the checkbox
                  ui.checked: whether or not the input was checked or unchecked (boolean) */
                  console.log(ui.value);
                  console.log(ui.text);

                // Add graph functionalities to the checkbox when checked/unchecked
                  if (ui.checked){
                    Drupal.behaviors.graspSectorMW.istSOSGetObservations(ui.value, element+'_op_graph');
                    // Plotly.addTraces(element+'_op_graph', obj);
                    Plotly.redraw(element+'_op_graph');
                  } else {
                    var check = ui.text;
                    $.each(Drupal.behaviors.graspSectorMW.traces, function(key, val){
                      if (val.name == check) {
                        // Drupal.behaviors.graspSectorMW.traces.splice(key,1);
                        Plotly.deleteTraces(element+'_op_graph', key);
                        // Plotly.redraw(element+'_op_graph');
                      }
                    });
                  }
                });
              // get the default selected (the first one) alternative or observed property OP
              var deafaultOP = $('#'+element+'_op_list > select').multiselect("getChecked").map(function(){
                return this.value;
              }).get();
              console.log(deafaultOP[0]);
              // Fill the traces array
              Drupal.behaviors.graspSectorMW.istSOSGetObservations(deafaultOP[0], element+'_op_graph');
              // NEW Plotly Plot

              var d3 = Plotly.d3;
              var WIDTH_IN_PERCENT_OF_PARENT = 90,
                  HEIGHT_IN_PERCENT_OF_PARENT = 70;

              var gd3 = d3.select('#'+element+'_op_graph')
                  // .append('div')
                  .style({
                      width: WIDTH_IN_PERCENT_OF_PARENT + '%',
                      // 'margin-left': (100 - WIDTH_IN_PERCENT_OF_PARENT) / 2 + '%',
                      'margin-left': 0 + '%',

                      height: HEIGHT_IN_PERCENT_OF_PARENT + 'vh',
                      // 'margin-top': (100 - HEIGHT_IN_PERCENT_OF_PARENT) / 2 + 'vh'
                      'margin-top': 0 + 'vh'
                  });

              var gd = gd3.node();

              Plotly.newPlot(gd, Drupal.behaviors.graspSectorMW.traces, Drupal.behaviors.graspSectorMW.layout);

              window.onresize = function() {
                Plotly.Plots.resize(gd);
              };

              Plotly.Plots.resize(gd);
          } else {
            alert(pjd.message);
          }
        };
      };

      // The first value opDef is passed directly because we got the checkbox value once clicked
      // The second is passed directly because we can plot on two different modal
      Drupal.behaviors.graspSectorMW.istSOSGetObservations = function(opDef, elem){
          console.log('THIS');
          console.log(elem);
          if ( elem == 'Component_op_graph'){
            var graphType = 'grasp_com_graph_type';
          } else {
            var graphType = 'grasp_sec_graph_type';
          }
          ///
          $.ajax({
              url: "?q=grasp_sector_ph_getobservation",
              type: "POST",
              async : false,
              // TODO - reintroduce offename in order to plot bar or linear charts
              // data: {offename: offeName, procname: Drupal.behaviors.graspSectorMW.PRCName, opdef: opDef, start: start, end: end},
              data: {offename: Drupal.behaviors.graspSectorMW.PRCOffering,
                     procname: Drupal.behaviors.graspSectorMW.PRCName,
                     opdef: opDef,
                     start: Drupal.behaviors.graspSectorMW.begin,
                     end: Drupal.behaviors.graspSectorMW.end,
                     graphtype: graphType},
              success: observationSuccess,
              // complete: completeFunction,
          });
          function observationSuccess (data, textStatus, jqXHR) {
            // console.log(data);
            var pjd = $.parseJSON(data)
            console.log(pjd);
            if (pjd.response == 'success'){
              var elements = pjd.observations.result.DataArray.elementCount;
              // use d3.transpose to format the returned array in a way usable with plotly.js
              var transposed = Plotly.d3.transpose(pjd.observations.result.DataArray.values);
              // transposed[0] contains the list of dates
              // transposed[n] contains the list of values or quality index

              //
              // Convert iso8601 dates to plotly dates
              // This is manadatory to use the type:"date" on xaxis
              //
              var timearr = []
              console.log(transposed);
              $.each(transposed[0], function(index, value){
                // TODO the date returned from istsos is +00:00 the date calculate is ported to the local system date
                // console.log(value); // 2015-05-03T14:40:00+00:00
                var d = new Date(value);
                // console.log(d); // Sun May 03 2015 16:40:00 GMT+0200 (CEST)
                var datestring = Drupal.behaviors.istsosUtils.ISODateString(d);
                timearr.push(datestring)
              });

              // Bar graph type
              if (pjd.graphtype == 'bar' || Drupal.behaviors.graspSectorMW.PRCOffering.indexOf("horizon") != -1 || Drupal.behaviors.graspSectorMW.PRCOffering == 'yearly'){
                  for (var count = 0; count<elements; count++ ) {
                     if (count%2 != 0){
                       var obj = {"x" : timearr, "y" : transposed[count] , "name" : pjd.observations.result.DataArray.field[count].name.split(":")[1], type:'bar' /*, marker: {color: 'rgb(142,124,195)'}*/ }
                       Drupal.behaviors.graspSectorMW.traces.push(obj);
                     }
                     if (count == 1){
                       var uom = pjd.observations.result.DataArray.field[count].uom;
                     }
                  }
                  // Bild the layout for bar graph
                  Drupal.behaviors.graspSectorMW.layout = {
                    barmode: 'group',
                    hovermode: "x", //
                    xaxis: {title:'Time', type:"date", tickformat:"%Y/%m/%d", hoverformat: "%y/%m/%d %X %Z"},
                    yaxis: {title: uom[0]},
                    margin: {t: 20},
                    showlegend: true, // fixed legend
                  }
              } else { // Line graph type can
                for (var count = 0; count<elements; count++ ) {
                   if (count%2 != 0){
                     var obj = {"x" : timearr, "y" : transposed[count] , "name" : pjd.observations.result.DataArray.field[count].name.split(":")[1], mode:pjd.graphtype /*, marker: {color: 'rgb(142,124,195)'}*/ }
                     Drupal.behaviors.graspSectorMW.traces.push(obj);
                   }
                   if (count == 1){
                     var uom = pjd.observations.result.DataArray.field[count].uom;
                   }
                }
                // Bild the layout for line graph
                Drupal.behaviors.graspSectorMW.layout = {
                  hovermode: "x", //
                  xaxis: {title:'Time', type:"date", tickformat:"%Y/%m/%d", hoverformat: "%y/%m/%d %X %Z"},
                  yaxis: {title: uom[0]},
                  margin: {t: 20},
                  showlegend: true, // fixed legend
                }
              }

              console.log(Drupal.behaviors.graspSectorMW.traces)
            } else {
              alert(pjd.message);
            }
          };
      };

    }
  }
})(jQuery);
