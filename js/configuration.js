(function ($) {

Drupal.behaviors.graspIstsosConfiguration = {
  attach: function (context, settings) {
  	$('#edit-istsos-service').change( function(){
  		var optionSelected = $(this).find("option:selected");
  		var valueSelected  = optionSelected.val();
  		console.log(valueSelected);
  		$('#edit-istsos-services-enabled-'+valueSelected).prop('checked', true);
  	});
	}
}

})(jQuery);
