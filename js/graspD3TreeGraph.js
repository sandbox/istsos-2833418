(function ($) {

Drupal.behaviors.graspD3TreeGraph = {
  attach: function (context, settings) {

    var d3 = Plotly.d3;

  	var margin = {top: 20, right: 120, bottom: 20, left: 120},
  	width = 960 - margin.right - margin.left,
  	height = 800 - margin.top - margin.bottom;
  	var tree = d3.layout.tree().size([height, width]);

  	var i = 0,
  	duration = 750,
  	root,
  	svgContainer;

  	var diagonal = d3.svg.diagonal()
  			.projection(function(d) { return [d.y, d.x]; });

  	Drupal.behaviors.graspD3TreeGraph.D3TreeProjectGraph = function(nodeId, elem){
  			// nodeId is the value to be passed to the sector_json_tree callback
  			// elem is the element id to append the svg graph
  			svgContainer = d3.select(elem).append("svg")
  					.attr("width", width + margin.right + margin.left)
  					.attr("height", height + margin.top + margin.bottom)
  				.append("g")
  					.attr("transform", "translate(" + margin.left + "," + margin.top + ")");

  			d3.json("?q=project_json/"+nodeId, function(error, flare) {
  				if (error) throw error;

  				root = flare;
  				root.x0 = height / 2;
  				root.y0 = 0;

  				function collapse(d) {
  					if (d.children) {
  						d._children = d.children;
  						d._children.forEach(collapse);
  						d.children = null;
  					}
  				}

  				root.children.forEach(collapse);
  				update(root);
  			});

  			d3.select(self.frameElement).style("height", "800px");
  	};

  	/* Global utilities function for D3 based tree graph */
  	function update(source) {
  		// console.log("Updating Tree");
  		// console.log(source);
  		// Compute the new tree layout.
  		var nodes = tree.nodes(root).reverse(),
  				links = tree.links(nodes);

  		// Normalize for fixed-depth.
  		nodes.forEach(function(d) { d.y = d.depth * 180; });

  		// Update the nodes…
  		var node = svgContainer.selectAll("g.node")
  				.data(nodes, function(d) { return d.id || (d.id = ++i); });

  		// Enter any new nodes at the parent's previous position.
  		var nodeEnter = node.enter().append("g")
  				.attr("class", "node")
  				.attr("transform", function(d) { return "translate(" + source.y0 + "," + source.x0 + ")"; })
  				.on("click", click);

  		nodeEnter.append("circle")
  				.attr("r", 1e-6)
  				.style("fill", function(d) { return d._children ? "lightsteelblue" : "#fff"; });

  		nodeEnter.append("foreignObject")
  					// .attr('x', -100) // set label respect to circle
  					.attr('x', function(d) {
  						if (d.depth == 0) {
  							return -100
  						} else {
  							return d._children ? -100 : 10;
  						}
  					})
  					// .attr('y', 10)
  					.attr('y', function(d) {
  						if (d.depth == 0) {
  								return 10
  						} else {
  							return d._children ? 10 : -10;
  						}
  					})
  					.attr('width', 200)
  					.attr('height', 100)
  					.append("xhtml:div")
  					.attr("class", "tcentr")
  					.html(function(d) {
  							if (d.depth == 1 && d.parent.name == 'Sectors'){ // discriminare in base alle proprietà e chiamare la rispettiva funzione relativa ai sectors o indicators
  								return '<a href="#">'+d.name+'</a>';
  								// return '<a href="?q='+d.href+'">'+d.name+'</a>';
  							} else {
  								return d.name;
  							}
  					})
  					.on("click", function (d) { // apply a click function to the element in order to call the SectorMW function that will open the Sector_MW dilaog window
  							if (d.depth == 1 && d.parent.name == 'Sectors'){
  								SectorLinkPH(d);
  							}
  					 });

  		// Transition nodes to their new position.
  		var nodeUpdate = node.transition()
  				.duration(duration)
  				.attr("transform", function(d) { return "translate(" + d.y + "," + d.x + ")"; });

  		nodeUpdate.select("circle")
  				.attr("r", 4.5)
  				.style("fill", function(d) { return d._children ? "lightsteelblue" : "#fff"; });

  		nodeUpdate.select("text")
  				.style("fill-opacity", 1);

  		// Transition exiting nodes to the parent's new position.
  		var nodeExit = node.exit().transition()
  				.duration(duration)
  				.attr("transform", function(d) { return "translate(" + source.y + "," + source.x + ")"; })
  				.remove();

  		nodeExit.select("circle")
  				.attr("r", 1e-6);

  		nodeExit.select("text")
  				.style("fill-opacity", 1e-6);

  		// Update the links…
  		var link = svgContainer.selectAll("path.link")
  				.data(links, function(d) { return d.target.id; });

  		// Enter any new links at the parent's previous position.
  		link.enter().insert("path", "g")
  				.attr("class", "link")
  				.attr("d", function(d) {
  					var o = {x: source.x0, y: source.y0};
  					return diagonal({source: o, target: o});
  				});

  		// Transition links to their new position.
  		link.transition()
  				.duration(duration)
  				.attr("d", diagonal);

  		// Transition exiting nodes to the parent's new position.
  		link.exit().transition()
  				.duration(duration)
  				.attr("d", function(d) {
  					var o = {x: source.x, y: source.y};
  					return diagonal({source: o, target: o});
  				})
  				.remove();

  		// Stash the old positions for transition.
  		nodes.forEach(function(d) {
  			d.x0 = d.x;
  			d.y0 = d.y;
  		});
  	}

  	// Toggle children on click.
  	function click(d) {
  		// console.log('d cliccked');
  		// console.log(d);
  		if (d.children) {
  			d._children = d.children;
  			d.children = null;
  		} else {
  			d.children = d._children;
  			d._children = null;
  		}
  		update(d);
  	}

  	/**/
    /* Functionaities when cliccking on a sector link from Project Hierachy tree graph */
    /**/
  	function SectorLinkPH(data){
  			console.log('SectorLinkPH clicked');
  			console.log(data.name);
  			// Get values from Tree Graph link data
  			var SectorName = data.name;
        // set the global variable for the sector name inside the graspSectorMW behavior
        Drupal.behaviors.graspSectorMW.SectorName = data.name;
  			var SectorNodeUrl = data.href; // !!! Global
        Drupal.behaviors.graspSectorMW.SectorNodeUrl = data.href;
  			var nid = data.href.split("/"); // node id of the sector content
  			var SectorNid = nid[1];
        Drupal.behaviors.graspSectorMW.SectorNid = SectorNid;
        // Prepare the Sector Modal Window
        Drupal.behaviors.graspSectorMW.SectorMWPrepare()
  			// Call the tree graph function for the clicked sector
  			Drupal.behaviors.graspD3TreeGraph.D3TreeProjectGraph(SectorNid, '#SHTree');
  			// Fill the sector modal window
        Drupal.behaviors.graspSectorMW.SectorMWFill(SectorNid)
  		// 	$.ajax({
  		// 			url: "?q=grasp_sector_ph", // function sector_ph in sector.inc
  		// 			type: "POST",
  		// 			data: {nid: SectorNid},
  		// 			dataType:'json',
  		// 			async: false,
  		// 			success: sectorMwContent,
  		// 	});
      //
  		// 	function sectorMwContent (data, textStatus, jqXHR) {
  		// 		console.log('Sector callback data');
      //
  		// 		var pjd = $.parseJSON(data)
  		// 		console.log(pjd);
  		// 		$('#Sdescription').append( pjd.description );// pjd.description contains the sector description
      //
  		// 		/* 1 - Set an ul of Feature of Interest links and marker description */
  		// 		$("#SFOI").append('<ul>');
  		// 		$.each(pjd.markers_markup, function (namePrc, value) {
  		// 					// console.log(value);
  		// 					$("#SFOI").append(value);
  		// 		});
  		// 		$("#SFOI").append('</ul>');
      //
  		// 		/*  Attach click functionality to the Features of interest links */
  		// 		$('.foilink').click(function () {
  		// 			// Get values from the Features of interest link attributes
  		// 			console.log('link clicked');
  		// 			FOIName = $(this).text();// MarkerName or FOIName
  		// 			var FOIDescr = $(this).next().text();
  		// 			var nome = $(this).attr('name');
      //
  		// 			FOIShortLabel = $(this).attr('id');
  		// 			FOINid = $(this).attr('nid');
      //
  		// 			// close the sector dialog window - STEP 2 CLOSE
  		// 			$('#Sector_MW').modal( "hide" );
  		// 			// open the sub sector dialog window - STEP 3 OPEN
  		// 			$('#SSector_MW').modal('show');
  		// 			// Empty #SSHTree, #SSIndMark and #SSIndOther before to redraw
  		// 			$('#SSHTree').empty();
  		// 			$('#SSIndMark').empty();
  		// 			$('#SSIndOther').empty();
  		// 			// set the SubSector modal window title
  		// 			$('#SSLabel').text('Sector : '+ SectorName );
  		// 			// set the back button
  		// 			$('#BTStep2').html('Back to '+ SectorName +' sector');
  		// 			// Call the tree graph function for the clicked sub sector
  		// 			Drupal.behaviors.graspD3TreeGraph.D3TreeProjectGraph(SectorNid, '#SSHTree');
      //
  		// 			// get a list of procedures attached to this Sector
  		// 			// TODO vogliamo tutte le procedures di tutti i markers attached al settore selezionato
  		// 			// TODO pjd.markers_ids contiene la lista degli id dei marker
  		// 			// TODO modifichiamo il seguente ajax copiando da istsos_procedures_list.inc
  		// 			// $.ajax({
  		// 			// 	url: "?q=sector_node/procedures_list",
  		// 			// 	type: "POST",
  		// 			// 	data: {selectedfoiid: FOIid, foilist: pjd.markers_ids},
  		// 			// 	dataType:'json',
  		// 			// 	async: false,
  		// 			// 	success: subSectorDialog,
  		// 			// });
  		// 		});
  		// };
  	};


    }
  }
})(jQuery);
