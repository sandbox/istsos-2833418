(function ($) {
  Drupal.behaviors.compareGraph = {
    attach: function (context, settings) {

      Drupal.behaviors.compareGraph.tracesdg1 = Drupal.behaviors.compareGraph.tracesdg1 || [];
      Drupal.behaviors.compareGraph.tracesdg2 = Drupal.behaviors.compareGraph.tracesdg2 || [];
      Drupal.behaviors.compareGraph.PRCNamedg1 = Drupal.behaviors.compareGraph.PRCNamedg1 || {};
      Drupal.behaviors.compareGraph.PRCNamedg2 = Drupal.behaviors.compareGraph.PRCNamedg2 || {};

      $.ajax({
          url: "?q=graph_compare/proceduresgetfulllist", // compare_graph.inc
          type: "POST",
          async: false,
          data: {},
          success: fillProceduresTable,
          //error: errorFunction
      });

      function fillProceduresTable (data, textStatus, jqXHR) {
        var pjd = $.parseJSON(data);
        // console.log(pjd);
        // Add procedures names inside the scrollable table
        var stb = '';
        $.each(pjd.data, function (key, value) {
        stb += '<tr>';
        stb += '<td ><div id="' + value.name + '" class="draggable">' + value.name +'</div></td>';
        stb += '<td>' + value.description + '</td>';
        stb += '<td>' + value.offerings[1] + '</td>';
        stb += '</tr>';
        });
        $('#tbl_cnt tbody').append(stb);
      };

		/************************************************************************/
		/*  Manage multiple Alternatives call when click on the alternatives slectbox */
		/************************************************************************/

			function LoadSeriesMultiple(offeName, procNames, opName){
				console.log(offeName +'/'+ procNames +'/'+ opName);
				//-----------------------------------
							var offering = encodeURIComponent(offeName);
				//-----------------------------------
				//			var offering = 'daily';

				$.ajax({
						url: "?q=graph_compare/show", // compare_graph.inc
						type: "POST",
						async: false,
						data: {offename: offering, procname: procNames[0], opname: opName, serie: 1},
						success: newSucessFunction,
						//error: errorFunction
				});

				$.ajax({
						url: "?q=graph_compare/show", // compare_graph.inc
						type: "POST",
						async: false,
						data: {offename: offering, procname: procNames[1], opname: opName, serie: 2},
//						success: successFunction,
						success: newSucessFunction,
						//error: errorFunction
				});

				function newSucessFunction (data, textStatus, jqXHR) {
					if (data.error){
						alert(data.error);
					} else {

						if (data[4] == 1){ // graph one
							if (minYg1 == 0){
								minYg1 = data[2] - 0.1*(data[3]-data[2])
							} else if (minYg1 > data[2]) {
								minYg1 = data[2] - 0.1*(data[3]-data[2])
							}
							tseriesOne.push(data[1]);
						}
						if (data[4] == 2){ // graph two
							if (minYg2 == 0){
								minYg2 = data[2] - 0.1*(data[3]-data[2])
							} else if (minYg2 > data[2]) {
								minYg2 = data[2] - 0.1*(data[3]-data[2])
							}
							tseriesTwo.push(data[1]);
						}
					}
				}
			}
		/************************************************************************/
		/*                          END                                         */
		/************************************************************************/

		/* Set global variables */
			// var AlternativesArray = new Array;
			// var tseriesOne = new Array;
			// var tseriesTwo = new Array;
			// var graphid, graphRenderer;
			// var minYg1 = 0;
			// var minYg2 = 0;
			// var palette = new Rickshaw.Color.Palette( { scheme: 'spectrum14' } );

			// set the table to be scrollable
			// $('table.small-table').scrollTableBody({rowsToDisplay:4});

	    /* D&D */
	    $('.draggable').draggable( {
	    	containment: '#compare-graph-form',
	    	helper: myHelper,
	    });

	    function myHelper( event ) {
	    	console.log($(this).attr('id')); // the dragged element id
				return '<div id="draggableHelper">'+$(this).attr('id')+'</div>';
			}

			$('.droppable').droppable({
        over: function (event, ui) {
        var dropItem = $(this);
        var dragItem = $(ui.draggable);
        dropItem.html(dragItem.attr('id'));
        },
        out: function (event, ui) {
          var dropItem = $(this);
          dropItem.html('Drop here');
        },
		    drop: handleDropEvent
		  });

		  function handleDropEvent( event, ui ) {
				var draggable = ui.draggable;
				// $(this).append(draggable.attr('id'));

				console.log('droppable element');
				console.log($(this).attr('id'));// dg1 or dg2

				graphid = $(this).attr('id'); // id of graph dg1 or dg2
				var procName = draggable.attr('id');
        if (graphid == 'dg1'){
          Drupal.behaviors.compareGraph.PRCNamedg1 = procName;
          // TODO Fill the traces array Drupal.behaviors.compareGraph.tracesdg1
          // Drupal.behaviors.compareGraph.istSOSGetObservations(deafaultOP[0], 'chart'+graphid);
        }
        if (graphid == 'dg2'){
          Drupal.behaviors.compareGraph.PRCNamedg2 = procName;
          // TODO Fill the traces array Drupal.behaviors.compareGraph.tracesdg2
          // Drupal.behaviors.compareGraph.istSOSGetObservations(deafaultOP[0], 'chart'+graphid);
        }

        if (( !($.isEmptyObject(Drupal.behaviors.compareGraph.PRCNamedg1)) ) && ( !($.isEmptyObject(Drupal.behaviors.compareGraph.PRCNamedg2)) )) {
          console.log('ENTERING PlotlyFill');
          Drupal.behaviors.compareGraph.PlotlyFill('dg1', Drupal.behaviors.compareGraph.PRCNamedg1);
          // Drupal.behaviors.compareGraph.PlotlyFill('chartdg2', Drupal.behaviors.compareGraph.PRCNamedg2);
        }
			} // close the drop handler

      Drupal.behaviors.compareGraph.PlotlyFill = function (graphid, procName){
        // get a list of alternatives attached to this procedure
        console.log('PlotlyMWFill');
        $.ajax({
          url: "?q=grasp_sector_ph_op_list",
          data: {procname:procName, },
          type: "POST",
          dataType:'json',
          async: false,
          success: FillGraph,
        });

        function FillGraph (data, textStatus, jqXHR) {
          console.log('Compare Graph - FillGraph');
          var pjd = $.parseJSON(data)
          console.log(pjd);
            if (pjd.response == "success"){
              // TODO add graph_default_style
              // Drupal.behaviors.graspPlotly.DefaultStyle = bar;

              // set begin and end eventtime
              Drupal.behaviors.compareGraph.begin = pjd.eventtime[0];
              Drupal.behaviors.compareGraph.end = pjd.eventtime[1];
              //
              // Build the multiselect widget
              //
              if( $('#obspro_list').is(':empty') ) {
                var s = $('<select/>');
                $.each(pjd.op_list, function (opdef, opname){
                  // opname is name:name get just name
                  s.append($('<option value="'+opdef+'"/>').html(opname.split(":")[1]));
                });
                $('#obspro_list').append(s);

                $('#obspro_list > select').multiselect({
                  //appendTo: '#'+element+'_MW',
                  header: false,
                }).multiselectfilter();
              // attach the event handler when a checkbox is checked
                $('#obspro_list > select').on("multiselectclick", function(event, ui) {
                  /* event: the original event object
                  ui.value: value of the checkbox
                  ui.text: text of the checkbox
                  ui.checked: whether or not the input was checked or unchecked (boolean) */
                  console.log(ui.value);
                  console.log(ui.text);

                // Add graph functionalities to the checkbox when checked/unchecked
                  if (ui.checked){
                    Drupal.behaviors.compareGraph.istSOSGetObservations(ui.value, 'chart'+graphid);
                    // Plotly.addTraces(element+'_op_graph', obj);
                    Plotly.redraw(element+'_op_graph');
                  } else {
                    var check = ui.text;
                    $.each(Drupal.behaviors.compareGraph.traces+graphid, function(key, val){
                      if (val.name == check) {
                        Plotly.deleteTraces(element+'_op_graph', key);
                      }
                    });
                  }
                });
              }
              // get the default selected (the first one) alternative or observed property OP
              var deafaultOP = $('#obspro_list > select').multiselect("getChecked").map(function(){
                return this.value;
              }).get();
              console.log(deafaultOP[0]);
              // TODO Fill the traces array
              Drupal.behaviors.compareGraph.istSOSGetObservations(deafaultOP[0], 'chartdg1');
              Drupal.behaviors.compareGraph.istSOSGetObservations(deafaultOP[0], 'chartdg2');
              // NEW Plotly Plot

              var d3 = Plotly.d3;
              var WIDTH_IN_PERCENT_OF_PARENT = 90,
                  HEIGHT_IN_PERCENT_OF_PARENT = 50;

              // var gd3 = d3.select('#'+element+'_op_graph')
              var gd3 = d3.select('#chart'+graphid)
                  // .append('div')
                  .style({
                      width: WIDTH_IN_PERCENT_OF_PARENT + '%',
                      // 'margin-left': (100 - WIDTH_IN_PERCENT_OF_PARENT) / 2 + '%',
                      'margin-left': 0 + '%',

                      height: HEIGHT_IN_PERCENT_OF_PARENT + 'vh',
                      // 'margin-top': (100 - HEIGHT_IN_PERCENT_OF_PARENT) / 2 + 'vh'
                      'margin-top': 0 + 'vh'
                  });

              // if (graphid == 'dg1') {
              //   var gd1 = gd3.node();
              //   Plotly.newPlot(gd1, Drupal.behaviors.compareGraph.tracesdg1, Drupal.behaviors.compareGraph.layout);
              //
              //   window.onresize = function() {
              //     Plotly.Plots.resize(gd1);
              //   };
              //
              //   Plotly.Plots.resize(gd1);
              // }
              // if (graphid == 'dg2') {
              //   var gd2 = gd3.node();
              //   Plotly.newPlot(gd2, Drupal.behaviors.compareGraph.tracesdg2, Drupal.behaviors.compareGraph.layout);
              //
              //   window.onresize = function() {
              //     Plotly.Plots.resize(gd2);
              //   };
              //
              //   Plotly.Plots.resize(gd2);
            // }
                var gd = gd3.node();
                console.log("TRACES");
                console.log(Drupal.behaviors.compareGraph.tracesdg1);
                Drupal.behaviors.compareGraph.tracesdg2[0].xaxis = 'x2',
                Drupal.behaviors.compareGraph.tracesdg2[0].yaxis = 'y2',
                console.log(Drupal.behaviors.compareGraph.tracesdg2);
                Plotly.newPlot(gd, [Drupal.behaviors.compareGraph.tracesdg1[0], Drupal.behaviors.compareGraph.tracesdg2[0]], Drupal.behaviors.compareGraph.layout);

                window.onresize = function() {
                  Plotly.Plots.resize(gd);
                };

          } else {
            alert(pjd.message);
          }
        };
      };

      // The first value opDef is passed directly because we got the checkbox value once clicked
      // The second is passed directly because we can plot on two different modal
      Drupal.behaviors.compareGraph.istSOSGetObservations = function(opDef, elem){
        // console.log('elem');
        //   console.log(elem);
          if (elem == 'chartdg1'){
            Drupal.behaviors.compareGraph.PRCName = Drupal.behaviors.compareGraph.PRCNamedg1;
          }
          if (elem == 'chartdg2'){
            Drupal.behaviors.compareGraph.PRCName = Drupal.behaviors.compareGraph.PRCNamedg2;
          }
          // console.log(Drupal.behaviors.compareGraph.PRCName);
          $.ajax({
              url: "?q=grasp_sector_ph_getobservation",
              type: "POST",
              async : false,
              data: {offename: 'temporary',
                     procname: Drupal.behaviors.compareGraph.PRCName,
                     opdef: opDef,
                     start: Drupal.behaviors.compareGraph.begin,
                     end: Drupal.behaviors.compareGraph.end,
                     graphtype: 'none'},
              success: observationSuccess,
              // complete: completeFunction,
          });
          function observationSuccess (data, textStatus, jqXHR) {
            // console.log(data);
            var offeName = 'temporary';
            var pjd = $.parseJSON(data)
            console.log(pjd);
            if (pjd.response == 'success'){
              var elements = pjd.observations.result.DataArray.elementCount;
              // use d3.transpose to format the returned array in a way usable with plotly.js
              var transposed = Plotly.d3.transpose(pjd.observations.result.DataArray.values);
              // transposed[0] contains the list of dates
              // transposed[n] contains the list of values or quality index

              //
              // Convert iso8601 dates to plotly dates
              // This is manadatory to use the type:"date" on xaxis
              //
              var timearr = []
              console.log(transposed);
              $.each(transposed[0], function(index, value){
                // TODO the date returned from istsos is +00:00 the date calculate is ported to the local system date
                // console.log(value); // 2015-05-03T14:40:00+00:00
                var d = new Date(value);
                // console.log(d); // Sun May 03 2015 16:40:00 GMT+0200 (CEST)
                var datestring = Drupal.behaviors.istsosUtils.ISODateString(d);
                timearr.push(datestring)
              });

              // Bar graph type
              if (offeName.indexOf("horizon") != -1 || offeName == 'yearly'){
                  for (var count = 0; count<elements; count++ ) {
                     if (count%2 != 0){
                       var obj = {"x" : timearr, "y" : transposed[count] , "name" : pjd.observations.result.DataArray.field[count].name.split(":")[1], type:'bar' /*, marker: {color: 'rgb(142,124,195)'}*/ }
                       if (elem == 'chartdg1'){
                         Drupal.behaviors.compareGraph.tracesdg1.push(obj);
                       }
                       if (elem == 'chartdg2'){
                         Drupal.behaviors.compareGraph.tracesdg2.push(obj);
                       }
                     }
                     if (count == 1){
                       var uom = pjd.observations.result.DataArray.field[count].uom;
                     }
                  }
                  // Bild the layout for bar graph
                  Drupal.behaviors.compareGraph.layout = {
                    barmode: 'group',
                    hovermode: "x", //
                    xaxis: {title:'Time', type:"date", tickformat:"%Y/%m/%d", hoverformat: "%y/%m/%d %X %Z"},
                    yaxis: {title: uom[0]},
                    margin: {t: 20},
                    showlegend: true, // fixed legend
                  }
              } else { // Line graph type can
                for (var count = 0; count<elements; count++ ) {
                   if (count%2 != 0){
                     var obj = {"x" : timearr, "y" : transposed[count] , "name" : pjd.observations.result.DataArray.field[count].name.split(":")[1], mode:pjd.graphtype /*, marker: {color: 'rgb(142,124,195)'}*/ }
                     if (elem == 'chartdg1'){
                       Drupal.behaviors.compareGraph.tracesdg1.push(obj);
                     }
                     if (elem == 'chartdg2'){
                       Drupal.behaviors.compareGraph.tracesdg2.push(obj);
                     }
                   }
                   if (count == 1){
                     var uom = pjd.observations.result.DataArray.field[count].uom;
                   }
                }
                // Bild the layout for line graph
                Drupal.behaviors.compareGraph.layout = {
                  hovermode: "x", //
                  xaxis: {title:'Time', type:"date", tickformat:"%Y/%m/%d", hoverformat: "%y/%m/%d %X %Z"},
                  yaxis: {title: uom[0]},
                  margin: {t: 20},
                  showlegend: true, // fixed legend
                }
              }
              // console.log(Drupal.behaviors.compareGraph.traces)
            } else {
              alert(pjd.message);
            }
          };
      };


    }
  };
})(jQuery);
