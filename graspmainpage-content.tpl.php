<?php
/**
 * @file template file for 'graspmainpage-content' theme
 *
 * Available variables are:
 *   $myvar1
 *   $myvar2 (array with the following keys -- 'option1', and 'option2')
 *   $myobj (object)
 *
 *  TODO add bootstrap logic here
 */
?>
<!-- <div> -->
  <div><?php //print $myvar1; ?></div>
  <div><?php //print $myvar2['option1']; ?></div>
  <div><?php //print $myobj->thisData; ?></div>
<!-- </div> -->
<!-- Modified template -->
<?php if (user_is_logged_in()) : ?>
  <?php $maps = openlayers_maps(); if(variable_get('grasp_analysts_map', FALSE) && isset($maps[variable_get('grasp_analysts_map')]->data['behaviors']['ol_grasp_slidebar']) ): ?>
    <!-- Slider -->
      <div class="toggler">
      <a href="#" id="trigger" class="trigger" style="position: relative;"></a>
      <div id="panel1" class="panel" >
          <!-- Search box for markers names -->
          <div class="container-inline input-group">
            <input type="text" id="search" class="form-control" placeholder="Search for marker names">
            <span class="input-group-btn">
              <button id="search_submit" class="btn btn-default" type="button"><span class="icon glyphicon glyphicon-search" aria-hidden="true"></span></button>
            </span>
          </div><!-- /input-group -->
          <div class="container-inline" style="margin-top:10px;"><ul id="search_results"></ul></div>

          <hr style="width:90%;"></hr>

          <!-- Filter components and sectors -->
          <div>
            <div class="container-inline form-wrapper">
              <label>Filter</label>
            </div>
            <div class="container-inline form-wrapper">
              <input type="checkbox" id="sector" value="Sector" checked='checked' /> Sector<br />
              <input type="checkbox" id="component" value="Component" checked='checked' /> Component
            </div>
          </div>

          <hr style="width:90%;"></hr>

          <!-- Browse section -->
          <div>
            <div class="container-inline form-wrapper">
              <label>Browse</label>
            </div>
            <div class="container-inline form-wrapper">
              <a href="#" data-toggle="modal" data-target="#ProjectTree_MW">Project Hierarchy</a><br />
              <a href="?q=grasp_markers_list">Markers list</a><br />
              <a href="?q=grasp_sectors_list">Sectors list</a><br />
              <a href="?q=grasp_indicators_list">Indicators list</a><br />
              <a href="?q=grasp_procedures_list">Procedures list</a><br />
            </div>
          </div>

          <!-- Select scenario section -->
          <?php if (user_is_logged_in()): ?>
            <hr style="width:90%;"></hr>
            <div>
              <div class="container-inline form-wrapper">
                <label>Select Scenario</label>
              </div>
              <div class="container-inline form-wrapper">
                <?php
                  if (isset($_COOKIE['Drupal_visitor_istsosscheme'])){
                    $sel_service = $_COOKIE['Drupal_visitor_istsosscheme'];
                  } else {
                    $sel_service = variable_get('istsos_service');
                  }
                  $show_ser = grasp_get_available_services();
                  if ($show_ser){
                    foreach ($show_ser as $service){
                      if ($service !== 0){
                        /* query the datatabase for the services descriptions */
                        $service_desc = grasp_get_service_description($service);
                        if ($sel_service == $service){
                          print '<input type="radio" name="istsos_service" value="'.$service.'" description="'.$service_desc.'" checked="checked">'.$service_desc.'<br />';
                        } else {
                          print '<input type="radio" name="istsos_service" value="'.$service.'" description="'.$service_desc.'">'.$service_desc.'<br />';
                        }
                      }
                    }
                  } else {
                    drupal_set_message("To use grasp module you need to create and configure istsos services following this <a href='?q=admin/config/istsos/services/select'> link</a>.", 'error');
                  }
                ?>
              </div>
            </div>
          <?php endif; ?>
      </div>
    </div>
    <!-- END Slider -->
    <?php
      if(array_intersect(array('administrator', 'analyst' ), $user->roles)) {
        print openlayers_render_map(variable_get('grasp_analysts_map'));
      } else {
        // TODO verify if we want this anymore
        print openlayers_render_map(variable_get('grasp_stakeholders_map'));
      }
    ?>

  <?php else : ?>
    <?php drupal_set_message("To use grasp module you need to associate a map to the grasp module following this <a href='?q=admin/config/grasp/grasp_main_configuration'> link</a>.<br /> Apply all the Grasp behaviors to the selected map.", 'error');?>
  <?php endif;?>
<?php endif; ?>

<!--  -->
<!-- BOOTSTRAP MODAL WINDOWS  -->
<!--  -->
<!-- Modal Project Hierarchy -->
<div class="modal fade" id="ProjectTree_MW" tabindex="-1" role="dialog" aria-labelledby="PHLabel" aria-hidden="true">
  <div class="modal-dialog modal-wide" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="PHLabel">Project Hierarchy</h4>
      </div>
      <div class="modal-body">
        <div id="PHTree"></div> <!-- Project Hierarchy tree graph container -->
      </div>
      <!-- <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div> -->
    </div>
  </div>
</div>
<!-- END Modal Project Hierarchy -->

<!-- Modal Sector_MW -->
<div class="modal fade" id="Sector_MW" tabindex="-1" role="dialog" aria-labelledby="SLabel" aria-hidden="true">
  <div class="modal-dialog modal-wide" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="SLabel"></h4>
      </div>
      <div class="modal-body">
        <a href="#" id="BTStep1">Back to Project Hierarchy</a> <!-- Back link -->
        <div id="Sdescription"></div> <!-- Sector global description -->

        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingOne">
              <h4 class="panel-title">
                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSHTree" aria-expanded="true" aria-controls="collapseSHTree">
                  Sector criteria hierarchy
                </a>
              </h4>
            </div>
            <div id="collapseSHTree" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
              <div class="panel-body">
                <div id="SHTree"></div> <!-- Sector Hierarchy tree graph container -->
              </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingTwo">
              <h4 class="panel-title">
                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFOI" aria-expanded="false" aria-controls="collapseFOI">
                  Feature of interest inside this Sector
                </a>
              </h4>
            </div>
            <div id="collapseFOI" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
              <div class="panel-body">
                <div id="SFOI" ></div> <!-- Sector accordion with Features of interest attached to the sector  -->
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>

<!-- Modal SSector_MW -->
<div class="modal fade" id="SSector_MW" tabindex="-1" role="dialog" aria-labelledby="SSLabel" aria-hidden="true">
  <div class="modal-dialog modal-wide" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="SSLabel"></h4>
      </div>
      <div class="modal-body">
        <a href="#" id="BTStep2"></a> <!-- Back link -->
        <div id="SSdescription"></div>
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingOne">
              <h4 class="panel-title">
                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSSHTree" aria-expanded="true" aria-controls="collapseSSHTree">
                  Sector criteria hierarchy
                </a>
              </h4>
            </div>
            <div id="collapseSSHTree" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
              <div class="panel-body">
                <div id="SSHTree"></div> <!-- Sector Hierarchy tree graph container -->
              </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingTwo">
              <h4 class="panel-title">
                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseIndicator" aria-expanded="false" aria-controls="collapseIndicator">
                  Indicators list for this marker
                </a>
              </h4>
            </div>
            <div id="collapseIndicator" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
              <div class="panel-body">
                <ul id="SSIndMark" ></ul> <!-- Sector accordion with Features of interest attached to the sector  -->
              </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingThree">
              <h4 class="panel-title">
                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                  Other Indicators
                </a>
              </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
              <div class="panel-body">
                <ul id="SSIndOther" ></ul> <!-- Sector accordion with Features of interest attached to the sector  -->
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>

<!-- Modal Indicator_MW -->
<!-- TODO a -- graph_default_style add default for graph style (bar line) at the "op_graph" div-->
<div class="modal fade" id="Indicator_MW" tabindex="-1" role="dialog" aria-labelledby="IndLabel" aria-hidden="true">
  <div class="modal-dialog modal-wide" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="IndLabel"></h4>
      </div>
      <div class="modal-body">
        <a href="#" id="BTStep3"></a> <!-- Back link -->

        <div class="panel-group" id="indacc" role="tablist" aria-multiselectable="true">
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingInd">
              <h4 class="panel-title">
                <a id="indDesc" role="button" data-toggle="collapse" data-parent="#indacc" href="#collapseInd" aria-expanded="true" aria-controls="collapseInd">
                </a>
              </h4>
            </div>
            <div id="collapseInd" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingInd">
              <div class="panel-body">
                <div id="indLongDesc"></div>
              </div>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-sm-3">
            <div id="frequency_list"></div>
            <div id="Indicator_op_list"></div>
          </div>
          <div id="Indicator_op_graph" class="col-sm-9">
            <!-- TODO if you use nested div here down the graph is smaller -->
            <!-- <div id="indicator_op_graph"></div> -->
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Modal Component_MW -->
<div class="modal fade" id="Component_MW" tabindex="-1" role="dialog" aria-labelledby="ComponentLabel" aria-hidden="true">
  <div class="modal-dialog modal-wide" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="ComponentLabel"></h4>
      </div>
      <div class="modal-body">
        <a href="#" id="BTStep3"></a> <!-- Back link -->

        <div class="panel-group" id="compacc" role="tablist" aria-multiselectable="true">
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingComp">
              <h4 class="panel-title">
                <a id="compDesc" role="button" data-toggle="collapse" data-parent="#compacc" href="#collapseComp" aria-expanded="true" aria-controls="collapseComp">
                </a>
              </h4>
            </div>
            <!-- <div id="collapseComp" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingComp"> -->
            <div id="collapseComp" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingComp">
              <div class="panel-body">
                <div id="compLongDesc"></div>
              </div>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-sm-3">
            <h4>Variables List</h4>
            <section class="rsside"><div id="comp_prc_list"></div></section>
            <h4>Alternatives</h4>
            <section class="rsside"><!-- <div>Alternatives</div> --> <div id="Component_op_list"></div></section>
          </div>
          <div id="Component_op_graph" class="col-sm-9">
            <!-- TODO compare with line 295 if you use nested div here down the graph is smaller -->
            <!-- <div id="Component_op_graph"></div> -->
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- END  Modified template -->
