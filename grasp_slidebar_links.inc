<?php


/* Menu link called from the lateral slidebar - Procedures list link */
function grasp_procedures_list(){
  return drupal_get_form("grasp_procedures_form");
}

function grasp_procedures_form($form,&$form_state){
	$url     = variable_get('istsos_url');
	$service = $_COOKIE['Drupal_visitor_istsosscheme'];
  // Get the service description
  $service_desc = grasp_get_service_description($service);
  if (is_string($service_desc)) {
    drupal_set_title("Service $service - $service_desc");
  } else {
    drupal_set_title("Service $service");
  }

	$proc_get_list= istsos_REST_proceduregetlist($url, $service);
  // drupal_set_message("Procedure list <pre>".print_r($proc_get_list,TRUE)."</pre>");
  $proc_list = array();
  if (!empty($proc_get_list['data'])){
    foreach ($proc_get_list['data'] as $value){
      $proc_list[$value['name']] = array(
  			'name' => array( 'data' => array('#markup' => $value['name'])),
  			'description' => array( 'data' => array('#markup' => $value['description'])),
  		);
      $proc_details = istsos_REST_proceduredetails($url, $service, $value['name']);
      // drupal_set_message("Procedure details <pre>".print_r($proc_details['data']['location']['properties']['name'],TRUE)."</pre>");
      $proc_list[$value['name']]['location'] =  array( 'data' => array('#markup' => $proc_details['data']['location']['properties']['name']));
    }
  }

  $form['procedure_table'] = array(
    '#markup' => 'tableselect',
	);

	$header = array(
		'name'  => t('Procedure Name'),
		'location'	 => t('FOI name'),
		'description'  => t('Procedure Description'),
	);

	$form['procedure_table'] = array(
    '#type' => 'tableselect',
		'#header' => $header,
    '#options' => $proc_list,
		'#empty' => t('No procedures found'),
	);

	return $form;

}
