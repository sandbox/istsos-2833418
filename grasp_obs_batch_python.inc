<?php

function grasp_obs_batch(){
	return drupal_get_form("grasp_obs_batch_form");
}

function grasp_obs_batch_form($form,&$form_state){
	$form = array();

	$form['csvupload'] = array(
		'#title' => t('Upload observations csv file'),
		'#type' => 'managed_file',
		'#description' => t('The csv file must respect the standard for this project.'),
		'#upload_validators' => array('file_validate_extensions' => array('csv') ),
		'#upload_location' => 'public://',
	);

	$form['indicator_desc'] = array(
		'#type' => 'checkbox',
	  '#title' => t('Override indicator description if exists.'),
	);

	$form['submit'] = array('#type' => 'submit', '#weight' => 20, '#value' => t('Verify and insert data'));

	return $form;
}

function grasp_obs_batch_form_submit($form, &$form_state) {
	// drupal_set_message('<pre>'.print_r($form_state['values'],TRUE).'</pre>');
	$batch = array(
    'title' => t('Importing CSV ...'),
    'operations' => array(),
    'init_message' => t('Importing'),
#    'progress_message' => t('Processed @current out of @total.'),
    'error_message' => t('An error occurred during processing'),
    'finished' => 'grasp_import_finished',
    'file' => drupal_get_path('module', 'grasp') .'/grasp_obs_batch_python.inc',
  ) ;

	if ( isset( $form_state['values']['csvupload'] ) ) {
    // set the filename
    $batch['operations'][] = array('_grasp_set_filename', array($form_state['values']['csvupload'], $form_state['values']['indicator_desc']) );
		// get the headers and set parameters
		$batch['operations'][] = array('_grasp_get_header', array() );
		// Create indicators nodes if necessary
		$batch['operations'][] = array('_grasp_create_indicator', array() );
	}

  batch_set($batch);
}

function _grasp_set_filename($filename, $override, &$context){
	$context['results']['uploaded_filename'] = $filename;
	$context['results']['override_ind_desc'] = $override;
}


function _grasp_get_header(&$context){
	$fid = $context['results']['uploaded_filename'];
	$file = file_load($fid);
	$uri = $file->uri;
	$filepath = drupal_realpath($uri);

	// get variables to pass to the python script
	$url     = variable_get('istsos_url');

	global $databases;
	$data['user'] = $databases['default']['default']['username'];
	$data['password'] = $databases['default']['default']['password'];
	$data['dbname'] = $databases['default']['default']['database'];
	$data['host'] = $databases['default']['default']['host'];
	// $data['port'] = "5432";// TODO the port is hard coded -
	// drupal_set_message('DB <pre>'.print_r($data,TRUE).'</pre>');

	$status = exec('cd '.drupal_get_path('module', 'grasp').'; ./load_single_procedure.py '.$filepath.' '.$url.' '.$data['user'].' '.$data['password'].' '.$data['dbname'].' '.$data['host'].' 2>&1',$output, $status);
	// drupal_set_message('STATUS <pre>'.print_r($status,TRUE).'</pre>'); // IOError: [Errno 13] Permission denied: '/usr/local/istsos/logs/websingleimport.log'
	// drupal_set_message('OUTPUT <pre>'.print_r($output,TRUE).'</pre>');
	// drupal_set_message('cd '.drupal_get_path('module', 'grasp').'; ./load_single_procedure.py '.$filepath.' '.$url.' '.$data['user'].' '.$data['password'].' '.$data['dbname'].' '.$data['host'].'');
	foreach($output as $value) {
		if (strpos($value,'-ERROR-') !== false) {
    	drupal_set_message($value,'error');
			$context['results']['error'] = true;
		}
		if (strpos($value,'INDICATOR-') !== false) {
    	$elem = explode('-', $value);
			$csv = $elem[1];
			$appo = explode(',', $csv);
			$context['results']['prc_name'] = $appo[0];
			$context['results']['description'] = $appo[1];
			$context['results']['createindicator'] = true;
		}
	}

}

function _grasp_create_indicator (&$context) {
	if (!$context['results']['error'] && $context['results']['createindicator']){
			$query = new EntityFieldQuery();
			$entities = $query->entityCondition('entity_type', 'node')
				->propertyCondition('type', 'indicator')
				->propertyCondition('title', $context['results']['prc_name'])
				->propertyCondition('status', 1)
				->range(0,1)
				->execute();

			if (empty($entities['node'])) {
				#	$node = node_load(array_shift(array_keys($entities['node'])));
				global $user;
				$node = new stdClass();
				$node->title = $context['results']['prc_name'];
				$node->type = "indicator";
				node_object_prepare($node); // Sets some defaults. Invokes hook_prepare() and hook_node_prepare().
				$node->language = LANGUAGE_NONE; // Or e.g. 'en' if locale is enabled
				$node->uid = $user->uid;
				$node->status = 1; //(1 or 0): published or not
				$node->promote = 0; //(1 or 0): promoted to front page
				$node->comment = 0; // 0 = comments disabled, 1 = read only, 2 = read/write

				$node->field_ind_desc_short[$node->language][0]['value'] = $context['results']['description'];

				$node = node_submit($node); // Prepare node for saving
				node_save($node);
				$context['results']['indicator'] = $node->nid;
			} elseif ($context['results']['override_ind_desc'] == 1) {
				$node = node_load(array_shift(array_keys($entities['node'])));
				$node->field_ind_desc_short[$node->language][0]['value'] = $context['results']['description'];
				$node = node_submit($node); // Prepare node for saving
				node_save($node);
			}

	}
}

function grasp_import_finished($success, $results, $operations) {
	// drupal_set_message("Success <pre>".print_r($success,TRUE)."</pre>");
	// drupal_set_message("Results <pre>".print_r($results,TRUE)."</pre>");
	// drupal_set_message("Operations <pre>".print_r($operations,TRUE)."</pre>");
	if ($success){
		drupal_set_message("Import CSV finished.");
		if (isset($results['procedure'])){
			drupal_set_message("Created new procedure ");
		}
		if (isset($results['indicator'])){
			drupal_set_message("Created new indicator");
		}

	}
}
